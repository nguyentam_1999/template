import React, { useMemo, useState } from 'react'
import Button from './Button'
import SidebarMobile from './SidebarMobile';

const Header = () => {
    const [isShowSidebar, setShowSidebar] = useState(false);

    const width = useMemo(() => {
        return window.innerWidth;
    }, []);

    const showSidebarModal = ()=> {
        setShowSidebar(false);
      }

    return (
        <>
        <div className='w-full h-[50px] xl:h-[123px] fixed top-0 right-0 left-0 bg-white px-[24px] lg:px-[2vw]  2xl:px-[4vw] w-1920:px-[8vw]  !z-[20]'>
            <div className='flex justify-between items-center h-full uppercase text-[12px] 2xl:text-[16px]  font-medium'>
                <div onClick={()=>setShowSidebar(!isShowSidebar)} className='xl:hidden cursor-pointer'>
                    <img src='/images/menu_navbar.svg' alt='' />
                </div>
                <div>
                    <img className='w-[69px] h-[24px] xl:h-auto xl:w-auto' src='/images/logo.jpg' />
                </div>
                <div className=' hidden xl:flex [&>span]:ml-[24px] '>
                    <span className='border-b border-black'>Trang chủ</span>
                    <span>Giới thiệu</span>
                    <span>Sản phẩm</span>
                    <span>Dự án</span>
                    <span>Tin tức</span>
                    <span>Liên hệ</span>
                    <span>Tuyển dụng</span>
                    <span>Thư viện</span>
                </div>

                <div className='flex items-center xl:[&>div]:ml-[24px]'>
                        <div>
                        <svg width="22" height="21" viewBox="0 0 22 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path
                             d="M8.65604 6.85076e-09C10.231 -6.34322e-05 11.7746 0.440466 13.1127 1.27188C14.4508 2.10328 15.5301 3.29248 16.2288 4.70525C16.9275 6.11803 17.2178 7.69815 17.067 9.2673C16.9161 10.8364 16.3301 12.3322 15.375 13.5856L20.7185 18.9362C20.9205 19.1392 21.0378 19.4115 21.0466 19.6978C21.0553 19.9842 20.9548 20.2631 20.7655 20.478C20.5762 20.6929 20.3123 20.8276 20.0273 20.8548C19.7424 20.8819 19.4578 20.7995 19.2314 20.6243L19.1255 20.5307L13.7798 15.1824C12.7129 15.9966 11.4676 16.5449 10.147 16.7818C8.82641 17.0187 7.46842 16.9374 6.18542 16.5447C4.90242 16.1519 3.7313 15.459 2.76897 14.5233C1.80664 13.5876 1.08076 12.436 0.651397 11.1637C0.222034 9.89146 0.101532 8.53514 0.299862 7.20701C0.498193 5.87888 1.00965 4.61712 1.79192 3.52612C2.57419 2.43512 3.60478 1.54627 4.79841 0.933095C5.99203 0.319925 7.31439 7.0877e-05 8.65604 6.85076e-09ZM8.65604 2.25525C7.01269 2.25525 5.43664 2.90867 4.27462 4.07176C3.11259 5.23485 2.45977 6.81234 2.45977 8.4572C2.45977 10.1021 3.11259 11.6795 4.27462 12.8426C5.43664 14.0057 7.01269 14.6591 8.65604 14.6591C10.2994 14.6591 11.8754 14.0057 13.0375 12.8426C14.1995 11.6795 14.8523 10.1021 14.8523 8.4572C14.8523 6.81234 14.1995 5.23485 13.0375 4.07176C11.8754 2.90867 10.2994 2.25525 8.65604 2.25525Z" 
                             fill={`${width < 1280 ? "#3A5E81" : "#07A0A0"}`}/>
                        </svg>

                        </div>

                        <div className='hidden xl:block'>
                            <img src='/images/usa_icon.svg' alt='' />
                        </div>

                        <div className='hidden xl:block w-fit h-fit border border-blue-800 rounded-[4px]'>
                            <img src='/images/vn_icon.svg' alt='' />
                        </div>

                        <div className='hidden xl:block'>
                            <Button color='primary' className='rounded-[50px]'>Đăng nhập</Button>
                        </div>
                </div>
            </div>
        </div>
            <div className='xl:hidden'>
                <SidebarMobile isShow={isShowSidebar} showSidebarModal={showSidebarModal} />
            </div>
        </>
    )
}

export default Header