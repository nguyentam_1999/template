import clsx from "clsx";
import useInView from "../../hook/useInView";
import Button from "../Button";
import { useEffect, useMemo, useRef } from "react";
import Title from "./Title";

export default function TopicSuccess() {
  const { ref, isInView } = useInView();
  const refFrame = useRef<HTMLIFrameElement>(null);

  useEffect(() => {
    if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
      create_iframe();
    }
  }, [refFrame.current]);

  function create_iframe() {
    if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
      refFrame.current?.classList.add("ios");
    }
  }

  return (
    <div className="mt-[43px] md:mt-[100px]  " ref={ref}>
      <Title
        title="THÀNH CÔNG CỦA BẠN LÀ ĐỊNH HƯỚNG CỦA CHÚNG TÔI "
        isInView={isInView}
      />

      <div>
        <div
          className={clsx(
            "flex justify-between flex-col lg:flex-row h-max pl-[-20px] lssm:px-[24px] md:px-[80px]  xl:px-[50px]  w-1920:px-[162px] py-[37px] lg:py-[136px]"
          )}
          style={{
            backgroundImage:
              "linear-gradient(180deg, #CEFBFB 6.65%, rgba(253, 254, 255, 0) 100%)",
          }}
        >
          <div
            ref={refFrame}
            className={clsx(
              "sc<992:w-[60%]  relative sc991:w-full  pt-[20px] pl-[20px] lg:pt-[39px] lg:pl-[39px]",
              {
                "animate__animated animate__fadeInLeft": isInView,
              }
            )}
          >
            <div className="w-[88%] z-[-1] absolute top-0 left-0 bg-secondary-color rounded-tl-[10px] lg:rounded-tl-[30px] h-[50%]"></div>
            <div>
              <iframe
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                scrolling="no"
                allowFullScreen={true}
                className="sc991:w-full lssm:h-[300px]  md:h-[450px] lg:h-[610px] xl:h-[492px] md:w-full object-cover rounded-[10px]"
                width="100%"
                height="315"
                src={`https://www.youtube.com/embed/${
                  true ? "0sIktMTze1Y" : "aUatyOjy3A8"
                }`}
                title="Anfico"
              ></iframe>
            </div>
          </div>
          <div
            className={clsx(
              "w-[40%] sc<992:ml-[60px] flex flex-col justify-between  min-h-full sc991:w-full mt-[28px] lg:mt-0"
            )}
          >
            <div
              className={clsx("text-px14 lg:text-px16  text-justify", {
                "animate__animated animate__flash": isInView,
              })}
            >
              <div
                className={clsx(
                  "text-px14 lg:text-px18 text-justify text-text-secondary py-2"
                )}
              >
                <p>
                  Với tâm huyết của ban lãnh đạo, kinh nghiệm và quyết tâm của
                  toàn thể đội ngũ kỹ thuật, công nhân viên, chúng tôi luôn tạo
                  ra những sản phẩm có chất lượng, thẩm mỹ cao. Bên cạnh đó
                  không ngừng cập nhật nâng cấp công nghệ, quy trình sản xuất để
                  đáp ứng được mọi yêu cầu của Quý khách hàng gần xa cả về chất
                  lượng và tiến độ.
                </p>
                <p>
                  Công ty chú trọng tuyệt đối đến chất lượng sản phẩm thông qua
                  việc lựa chọn nhà cung cấp nguyên liệu uy tín, đầu tư trang
                  thiết bị máy móc hiện đại cùng với những kỹ sư giàu kinh
                  nghiệm, đội ngũ công nhân lành nghề và hệ thống quản lý khoa
                  học. Chúng tôi tự tin khẳng định sản phẩm của mình có thể sánh
                  ngang với các sản phẩm thuộc phân khúc cao cấp đến từ châu Âu.
                </p>
              </div>
            </div>
            <div className="sc>768:flex sc>768:justify-center mt-[28px] lg:mt-auto">
              <Button
                color="primary"
                className="text-px14 lg:text-[18px]  h-[50px] rounded-[5px] w-[130px] shadow-md"
              >
                <span className="flex items-center text-inherit font-medium">
                  Xem thêm
                </span>
              </Button>{" "}
            </div>
            <div></div>
          </div>
        </div>
      </div>
    </div>
  );
}
