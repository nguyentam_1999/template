import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
// import "swiper/modules/grid";
import "swiper/css/navigation";
import { Autoplay, Navigation, } from "swiper";
import React, { useMemo, useState, useEffect } from "react";
import Title from "./Title";
import useInView from "../../hook/useInView";
const LIMIT = 6;

export default function SliderProject() {


    const [projectListActive, setProject] = useState<any[]>([])
    const { isInView, ref } = useInView();
    const navigationPrevRef = React.useRef(null)
    const navigationNextRef = React.useRef(null)

    const width = useMemo(() => {
        return window.innerWidth;
    }, []);

    useEffect(() => {
        getData()
    }, [])


    const getData = async () => {
        const res = await (await fetch("api/project/priority")).json()

        setProject(res);
    }



    return (
        <div ref={ref} className="mt-[40px] lg:mt-[128px]">
            <Title title="Dự án" isInView={isInView} />
            <div className="px-[24px] md:px-[50px]  lg:px-[164px]">
            <div className="relative">
                <Swiper
                    slidesPerView={width >= 1280 ? 3 : (width >= 768 ? 2 : (width >= 380 ? 1 : (width >= 330 ? 1 : 1)))}
                    spaceBetween={30}
                    // slidesPerGroup={1}
                    // onRealIndexChange={(i)=> { setCurrentElement(i.activeIndex)}}
                    loop={false}
                    loopFillGroupWithBlank={true}
                    autoplay={{
                        delay: 2500,
                        disableOnInteraction: false,
                    }}
                    navigation={{
                        // Both prevEl & nextEl are null at render so this does not work
                        prevEl: navigationPrevRef.current,
                        nextEl: navigationNextRef.current,
                    }}
                    onSwiper={(swiper: any) => {
                        // Delay execution for the refs to be defined
                        setTimeout(() => {
                            // Override prevEl & nextEl now that refs are defined
                            swiper.params.navigation.prevEl = navigationPrevRef.current;
                            swiper.params.navigation.nextEl = navigationNextRef.current;

                            // Re-init navigation
                            swiper.navigation.destroy();
                            swiper.navigation.init();
                            swiper.navigation.update();
                        });
                    }}
                    modules={[Navigation, Autoplay]}
                    className="h-auto"
                >

                    {
                        projectListActive.map((item, index) => {
                            return (
                                <SwiperSlide className="max-h-fit" key={item.id}>
                                    <div>
                                        <div>
                                            <img src={`${item?.avatarUrl}`} className="lssm:min-h-[432px] xl:min-h-[432px] block  max-h-[432px]  min-w-full object-cover rounded-[10px] lg:mt-[24px]" alt={item?.avatarPath ?? ""} />
                                        </div>
                                        <div className="">
                                            <p className="text-text-blue text-[20px] font-semibold text-center  sc>768:text-px16 line-clamp-2  w-full mt-[24px]">{item.titleVi}</p>

                                        </div>

                                    </div>
                                </SwiperSlide>
                            )
                        })
                    }
                </Swiper>
                <div ref={navigationPrevRef} className="absolute top-[50%] left-[-20px] md:left-[-50px] lg:left-[-60px] cursor-pointer translate-y-[-50%] text-text-primary sc>768:text-[24px] md:text-[48px]">
                    <img className="w-[10px] h-[20px] lg:w-[23px] lg:h-[49px]" src="/images/arrowLeft_icon.svg" alt="" />
                </div>
                <div ref={navigationNextRef} className="absolute top-[50%] lssm:right-[-20px] md:right-[-50px] lg:right-[-60px]  cursor-pointer translate-y-[-50%] text-text-primary sc>768:text-[24px] md:text-[48px]">
                    <img className="w-[10px] h-[20px] lg:w-[23px] lg:h-[49px]" src="/images/arrowRight_icon.svg" alt="" />

                </div>
            </div>

            </div>
        </div>
    );
}


