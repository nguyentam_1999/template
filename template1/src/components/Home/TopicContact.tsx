import clsx from "clsx";

import Button from "../../components/Button";
import useInView from "../../hook/useInView";


export default function TopicContact() {

    const { ref, isInView } = useInView()

    return (
        <div className="lssm:h-auto sc<992:h-[579px]  max-w-full  lssm:px-[24px] md:pl-[80px] lg:pr-[70px]  xl:pl-[50px]  w-1920:pl-[162px] py-[27px] lg:py-[67px] relative" ref={ref}
            style={{
                backgroundImage: "linear-gradient(173.77deg, #D6E0DA 4.92%, rgba(214, 224, 218, 0) 94.36%)"
            }}
        >
            <div className="flex flex-col lg:flex-row lg:items-center">
                <div className="w-[100%] lg:w-[50%] h-auto flex flex-col justify-between text-text-title">
                    <div className="mb-[29px]">
                        <img src="/images/icon_contact.svg" alt="" />
                    </div>
                    <h2 className={clsx(" text-[32px] uppercase sc991:text-px20 font-bold", { "animate__animated animate__fadeInDown": isInView })}>LIÊN HỆ VỚI CHÚNG TÔI ĐỂ ĐƯỢC HỖ TRỢ NHANH CHÓNG NHẤT</h2>
                    <div className="sc991:flex  justify-between  mt-[15px] lg:mt-[24px] ">
                        <div className="w-[330px] h-[2px] bg-[#144E8C]" />
                    </div>
                    <p className={clsx("text-px14 md:text-px18 text-justify mt-[15px] lg:mt-[38px]  text-[#3A5E81]", { "animate__animated animate__fadeInLeftBig": isInView })}>Mong muốn mang đến giá trị to lớn cho đối tác, SME có đội ngũ nhiệt tình, chu đáo luôn sẵn sàng giải đáp những thắc mắc, đưa ra những giải pháp, thiết kế có thể làm hài lòng những khách hàng khó tính nhất.</p>
                    <div className={clsx("w-full mt-[30px] lg:mt-[26px]", { "animate__animated animate__fadeInTopRight": isInView })}>
                        <Button color="primary" className="py-[10] px-[20px] lg:py-[18px] lg:px-[42px] text-px14 lg:text-[18px] font-bold max-w-fit">Liên hệ</Button>
                    </div>
                </div>
                <div className={clsx(" px-[8px] py-[14px] mt-[30px] lg:mt-0 lg:px-[12px] lg:py-[20px] 2xl:px-[24px] 2xl:py-[36px] lg:w-[50%]  sc991:w-full sc<992:overflow-hidden sc991:h-auto h-fit   rounded-tl-[12px] lg:ml-[50px]  2xl:ml-[119px]", { "animate__animated animate__fadeInUp": isInView })}>
                    <div className="absolute top-0 z-[-1] right-0 w-[90%] rounded-tr-[30px] h-[33%] bg-secondary-color" />
                    <div>
                        <img src={`images/images_contact.jpg`} alt="contacer" className="h-full min-w-full object-cover rounded-[30px]" />
                    </div>
                    <div className="absolute bottom-0 left-0 z-[-1] w-[90%] rounded-bl-[30px] h-[33%] bg-secondary-color" />

                </div>
            </div>

        </div>
    )

}