import useInView from "../../hook/useInView";
import Title from "./Title";
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
// import "swiper/modules/grid";
import "swiper/css/navigation";
import { Autoplay, Navigation, Grid } from "swiper";
import React, { useMemo, useState, useEffect } from "react";

const TopicCustomer = () => {
  const { isInView, ref } = useInView();
  const [partners, setPartners] = useState<any[]>([]);

  const width = useMemo(() => {
    return window.innerWidth;
  }, []);

  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    const res = await (await fetch("api/partner")).json();
    setPartners(res);
  };

  const navigationPrevRef = React.useRef(null);
  const navigationNextRef = React.useRef(null);

  return (
    <div
      className="md:h-[356px]  sc>768:pb-[30px] mt-[20px] lg:mt-[100px]  flex flex-col"
      ref={ref}
    >
      <Title title="Khách hàng" isInView={isInView} />
      <div className="lssm:px-[24px] md:px-[80px]   xl:px-[50px]  w-1920:px-[162px]">
        <div className="flex-1 flex items-center  relative">
          <Swiper
            slidesPerView={
              width >= 1850
                ? 6
                : width >= 992
                ? 4
                : width >= 380
                ? 2
                : width >= 330
                ? 2
                : 1
            }
            spaceBetween={30}
            // slidesPerGroup={1}
            loop={false}
            loopFillGroupWithBlank={true}
            autoplay={{
              delay: 2500,
              disableOnInteraction: false,
            }}
            navigation={{
              // Both prevEl & nextEl are null at render so this does not work
              prevEl: navigationPrevRef.current,
              nextEl: navigationNextRef.current,
            }}
            onSwiper={(swiper: any) => {
              // Delay execution for the refs to be defined
              setTimeout(() => {
                // Override prevEl & nextEl now that refs are defined
                swiper.params.navigation.prevEl = navigationPrevRef.current;
                swiper.params.navigation.nextEl = navigationNextRef.current;

                // Re-init navigation
                swiper.navigation.destroy();
                swiper.navigation.init();
                swiper.navigation.update();
              });
            }}
            modules={[Navigation, Autoplay]}
            className="slider-company-home"
          >
            {partners.map((item, index) => {
              if ((index + 1) % 2 === 0 && width < 768) return "";
              return width < 768 ? (
                <SwiperSlide
                  className="slider-company-home-item "
                  key={item.id}
                >
                  <div className="sc>768:max-h-[74px]">
                    <img
                      src={item?.imageUrl ?? ""}
                      className="max-h-[90%]  object-contain w-[90%] h-full"
                      alt={item?.imageUrl ?? ""}
                    />
                  </div>
                  {index + 1 < partners.length && (
                    <div className="mt-[24px] sc>768:max-h-[74px]">
                      <img
                        src={partners[index + 1]?.imageUrl ?? ""}
                        className="max-h-[90%] object-contain w-[90%] h-full"
                        alt={partners[index + 1]?.imageUrl ?? ""}
                      />
                    </div>
                  )}
                </SwiperSlide>
              ) : (
                <SwiperSlide className="slider-company-home-item" key={item.id}>
                  <div className="">
                    <img
                      className="object-contain w-[90%] h-full max-h-[90%]"
                      src={item?.imageUrl ?? ""}
                      alt={item?.imageUrl ?? ""}
                    />
                  </div>
                </SwiperSlide>
              );
            })}
          </Swiper>

          <div
            ref={navigationPrevRef}
            className="absolute top-[50%] left-[-20px] md:left-[-50px] lg:left-[-30px] cursor-pointer translate-y-[-50%] text-text-primary sc>768:text-[24px] md:text-[48px]"
          >
            <img
              className="w-[10px] h-[20px] lg:w-[23px] lg:h-[49px]"
              src="/images/arrowLeft_icon.svg"
              alt=""
            />
          </div>
          <div
            ref={navigationNextRef}
            className="absolute top-[50%] lssm:right-[-20px] md:right-[-50px] lg:right-[-30px]  cursor-pointer translate-y-[-50%] text-text-primary sc>768:text-[24px] md:text-[48px]"
          >
            <img
              className="w-[10px] h-[20px] lg:w-[23px] lg:h-[49px]"
              src="/images/arrowRight_icon.svg"
              alt=""
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default TopicCustomer;
