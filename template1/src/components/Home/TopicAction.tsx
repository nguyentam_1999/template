import clsx from "clsx";
import { useEffect, useState } from "react";
import useInView from "../../hook/useInView";
import Button from "../Button";
import Title from "./Title";

export default function TopicAction() {
  const { ref, isInView } = useInView();
  const [categories, setCategories] = useState<any[]>([]);


  useEffect(() => {
    getData();
  }, [])


  const getData = async () => {
    const res = await (await fetch("api/categories")).json();
    setCategories(res.list)
  }



  return (
    <>
      <Title title="LĨNH VỰC HOẠT ĐỘNG " isInView={isInView} />

      <div
        className="h-full  bg-[#0C9191]"
        ref={ref}
      >
        <div className="flex flex-col lg:flex-row w-full h-full">
          <div className=" xl:mt-[24px] 2xl:mt-[50px] flex flex-col flex-1 lg:mr-[12px] 2xl:mr-[40px] px-[31px] py-[27px] lg:py-[40px] lg:pl-[44px] h-auto">
            <div className="w-[50%] h-[53px] lg:w-[40%] lg:h-[96px] mb-[30px] lg:mb-0 border-l-[4px] border-t-[4px]  lg:border-l-[8px] lg:border-t-[8px] border-secondary-color"></div>
            <div className="lg:pl-[50px] lg:pr-[20px] 2xl:pr-[50px]  w-1920:pl-[130px] w-1920:pr-[100px]">
              <h4 className="font-semibold text-px16 lg:text-[32px] text-white-color lssm:mb-[24px] lg:mb-[47px]">
                Chế tạo & cung cấp thiết bị ngành
              </h4>
              <div className="grid lssm:grid-cols-2  sm-480:grid-cols-3 lg:grid-cols-2 xl:grid-cols-3 gap-x-[12px] gap-y-[12px] lsm-320:gap-x-[25px] lsm-320:gap-y-[22px] lg:gap-x-[46px] lg:gap-y-[32px]">
                {categories.map((item) => {
                  return (
                    <div key={item.id} className="w-fit">
                      <div
                        className={clsx(
                          "w-[133px] max-w-full h-[137px] rounded-[10px] border-2 border-solid border-white-color flex items-center justify-center cursor-pointer",
                          { "animate__animated animate__fadeInDown": isInView }
                        )}
                      >
                        <img src={item.imageUrl} alt="" />
                      </div>
                      <p
                        className={clsx(
                          "text-px14  lg:text-px20 line-clamp-2  text-white-color mt-[14px] cursor-pointer",
                          { "animate__animated animate__flash": isInView }
                        )}
                      >
                        {item.nameVi}
                      </p>
                    </div>
                  );
                })}
              </div>

            </div>
            <div className="w-[50%] h-[53px] lg:w-[40%] lg:h-[96px] mt-[30px] lg:mt-0 border-r-[4px] border-b-[4px]  lg:border-r-[8px] lg:border-b-[8px] border-secondary-color ml-auto"></div>
          </div>

          <div
            className={clsx(
              "lg:w-[57%]",
              { "animate__animated animate__fadeInRight": isInView }
            )}
          >
            <img src={`/images/avatar_action.png`} className="w-full h-full object-cover" alt="" />
          </div>

        </div>
      </div>
    </>
  );
}
