import { useContext } from "react";
import Button from "./Button";
import { FormContact } from "./Home/FormContact";
const Footer = () => {
  return (
    <div className="bg-[#0E2C99] text-white">
      <div className="mx-auto 2xl:w-[1500px] w-1920:w-[1700px] w-full grid w-1920:grid-cols-[2fr_1fr_1fr] lg:grid-cols-3 grid-cols-1 gap-x-4 gap-y-6 lg:gap-y-2 lg:py-16 py-7 px-5">
        <div>
          <div className="h-full flex-1">
            <div className="rounded-[5px]">
              <img
                className="sc991:w-[120px] w-auto"
                src={`/images/logo.jpg`}
                alt="logo"
              />
            </div>
            <div className="flex items-center lg:items-baseline mt-5">
              <span className="text-inherit mr-5">
                <img
                  className="min-w-[14px]"
                  src="/images/icon_place.png"
                  alt=""
                />
              </span>
              <span className="w-fit lg:text-px18 text-px12">
                Tầng 4, Chung cư HH1 Meco Complex, Ngõ 102 Trường Chinh, P.
                Phương Mai, Q. Đống Đa, TP. Hà Nội
              </span>
            </div>
            <div className="flex items-center mt-5">
              <span className="text-inherit text-xl mr-5">
                <img
                  className="w-[14px]"
                  src="/images/icon_place_map.png"
                  alt=""
                />
              </span>
              <Button
                color="primary"
                className="lssm:w-[95px] lssm:h-[21px] lg:w-[125px] lg:h-[30px] lssm:text-[12px] lg:text-[16px] text-darkBlue !bg-white text-black rounded-[5px]"
              >
                Xem bản đồ
              </Button>
            </div>
          </div>
        </div>

        <div>
          <div className="text-px12 lg:text-px18">
            <h3 className="text-[12px] font-medium lg:text-[20px] lg:font-semibold text-inherit w-full">
              LIÊN HỆ
            </h3>
            <div className="lssm:mt-[7px] lg:mt-[32px] text-inherit sc991:flex sc991:flex-col">
              <div className="flex ">
                <span className="text-inherit text-xl w-[20px]">
                  <img
                    className="w-[20px]"
                    src="/images/phone-icon.png"
                    alt=""
                  />
                </span>
                <div>
                  <a href="tel:0976423099" className="ml-5">
                    +84 976 423 099
                  </a>{" "}
                  /{" "}
                  <a href="tel:0988740467" className="cursor-pointer">
                    +84 988 740 467
                  </a>
                </div>
              </div>
              <div className="flex mt-5 items-center">
                <span className="text-inherit text-xl w-[20px]">
                  <img
                    className="w-[20px]"
                    src="/images/email-icon.png"
                    alt=""
                  />
                </span>
                <a
                  href={`mailto:support@aladintech.co`}
                  target="blank"
                  className="ml-5"
                >
                  support@aladintech.co
                </a>{" "}
              </div>
              <div className="flex mt-5 items-center">
                <span className="text-inherit text-xl w-[20px]">
                  <img
                    className="w-[20px]"
                    src="/images/link_website.png"
                    alt=""
                  />
                </span>
                <a href="https://aladintech.co" target="blank" className="ml-5">
                  https://aladintech.co
                </a>{" "}
              </div>
              <div className="flex mt-5 items-center">
                <span className="text-inherit text-xl w-[20px] flex justify-center">
                  <img
                    className="lssm:w-[10px] sm:w-[15px]"
                    src="/images/facebook-icon.png"
                    alt=""
                  />
                </span>
                <a
                  href="https://www.facebook.com/aladintech.co"
                  target="blank"
                  className="ml-5 "
                >
                  https://www.facebook.com/aladintech.co
                </a>{" "}
              </div>
              {/* <div className="flex mt-5 items-center"><span className="text-inherit text-xl"><img src="/images/phone-icon.png" alt="" /></span> <Button onClick={showModalMap} color="empty" className="rounded-[10px] sc>768:text-px13 bg-white-color ml-5 text-text-primary">{t("footer.see_map")}</Button> </div> */}
            </div>
          </div>
        </div>

        <div>
          <div className="">
            <h3 className="text-[12px] font-medium lg:text-[20px]  lg:font-semibold text-inherit uppercase">
              ĐĂNG KÍ NHẬN TƯ VẤN MIỄN PHÍ NGAY
            </h3>
            <p className="text-[12px]">
              Thông tin khách hàng sẽ được HOANGLAM tuyệt đối bảo mật.
            </p>
            <FormContact />
          </div>
        </div>

        <div>
          <p className="text-white text-[10px] w-max uppercase text-center lg:text-left mx-auto lg:mx-0">
            <span className="font-bold">@2022 HOANGLAM </span>- A PRODUCT OF
            ALADIN TECHNOLOGY
          </p>
        </div>
      </div>
    </div>
  );
};

export default Footer;
