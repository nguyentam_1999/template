import useInView from "../../hook/useInView";
import Title from "./Title";
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
// import "swiper/modules/grid";
import "swiper/css/navigation";
import { Autoplay, Navigation, Grid } from "swiper";
import React, { useMemo, useState, useEffect } from "react";
import useViewport from "../../hook/useViewPort";

const TopicCustomer = () => {
  const { isInView, ref } = useInView();
  const [partners, setPartners] = useState<any[]>([]);

  const { width } = useViewport();

  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    const res = await (await fetch("api/partner")).json();
    setPartners(res);
  };

  const navigationPrevRef = React.useRef(null);
  const navigationNextRef = React.useRef(null);

  return (
    <div className="lg:pt-32 pt-8" ref={ref}>
      <div className="border-y border-r border-primary w-fit lg:py-4 py-2 lg:pr-3 pr-1 rounded-r-full">
        <div
          className="w-1920:ml-[calc((100vw-1500px)/2)] 2xl:ml-[calc((100vw-1300px)/2)] lg:ml-[calc((100vw-1000px)/2)] px-5 rounded-r-full lg:pr-36 pr-12"
          style={{
            backgroundImage:
              "linear-gradient(-45deg, #F7C1A399 23.31%, #FFFFFF0C 81.45%, #FFFFFF00 84.22%)",
          }}
        >
          <div
            className={`${
              isInView ? "animate__animated animate__fadeInRight" : ""
            }`}
          >
            <div className="text-text-primary font-bold uppercase text-px20 lg:text-px48">
              KHÁCH HÀNG
            </div>
          </div>
        </div>
      </div>
      <div className="">
        <div className="flex items-center py-14 px-5">
          <div className="lg:block hidden flex-1">
            <div className="w-full h-full flex items-center justify-center">
              <img
                ref={navigationPrevRef}
                className="w-[10px] lg:w-[23px] cursor-pointer"
                src="/images/arrowLeft_icon.svg"
                alt=""
              />
            </div>
          </div>
          <div className="w-1920:w-[1500px] 2xl:w-[1300px] lg:w-[1000px] w-full">
            <Swiper
              slidesPerView={width >= 1024 ? 6 : 3}
              spaceBetween={width >= 1024 ? 42 : 10}
              loop={true}
              loopFillGroupWithBlank={true}
              autoplay={{
                delay: 2500,
                disableOnInteraction: false,
              }}
              navigation={{
                // Both prevEl & nextEl are null at render so this does not work
                prevEl: navigationPrevRef.current,
                nextEl: navigationNextRef.current,
              }}
              modules={[Navigation, Autoplay]}
              className="slider-company-home"
            >
              {partners.map((item, index) => {
                return (
                  <SwiperSlide className="py-1" key={item.id}>
                    <div className="w-full aspect-video rounded-[10px] flex items-center justify-center">
                      <img
                        src={item?.imageUrl ?? ""}
                        className="max-h-[90%] object-contain w-[90%] h-full"
                        alt={item?.imageUrl ?? ""}
                      />
                    </div>
                  </SwiperSlide>
                );
              })}
            </Swiper>
          </div>
          <div className="lg:block hidden flex-1">
            <div className="w-full h-full flex items-center justify-center">
              <img
                ref={navigationNextRef}
                className="w-[10px] lg:w-[23px] cursor-pointer"
                src="/images/arrowRight_icon.svg"
                alt=""
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TopicCustomer;
