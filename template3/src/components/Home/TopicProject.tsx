import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
// import "swiper/modules/grid";
import "swiper/css/navigation";
import { Autoplay, Navigation, Pagination } from "swiper";
import React, { useMemo, useState, useEffect } from "react";
import Title from "./Title";
import useInView from "../../hook/useInView";
import useViewport from "../../hook/useViewPort";
const LIMIT = 6;

export default function SliderProject() {
  const [projectListActive, setProject] = useState<any[]>([]);
  const { isInView, ref } = useInView();
  const navigationPrevRef = React.useRef(null);
  const navigationNextRef = React.useRef(null);
  const paginationRef = React.useRef(null);

  const { width } = useViewport();

  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    const res = await (await fetch("api/project/priority")).json();

    setProject(res);
  };

  return (
    <div
      className="lg:pt-32 pt-8"
      ref={ref}
      style={
        width >= 1920
          ? {
              backgroundImage:
                "url(/images/icons/bg-dotted1.png),url(/images/icons/bg-dotted1.png)",
              backgroundRepeat: "no-repeat",
              backgroundPosition:
                "calc((100vw - 1500px) / 2 + 1500px) 128px, calc((100vw - 1500px) / 2 + 1345px) 0px",
            }
          : width >= 1536
          ? {
              backgroundImage:
                "url(/images/icons/bg-dotted1.png),url(/images/icons/bg-dotted1.png)",
              backgroundRepeat: "no-repeat",
              backgroundPosition:
                "calc((100vw - 1300px) / 2 + 1300px) 128px, calc((100vw - 1300px) / 2 + 1145px) 0px",
            }
          : width >= 1024
          ? {
              backgroundImage:
                "url(/images/icons/bg-dotted1.png),url(/images/icons/bg-dotted1.png)",
              backgroundRepeat: "no-repeat",
              backgroundPosition:
                "calc((100vw - 1000px) / 2 + 1000px) 128px, calc((100vw - 1000px) / 2 + 845px) 0px",
            }
          : {
              backgroundImage: "url(/images/icons/bg-dotted2.png)",
              backgroundRepeat: "no-repeat",
              backgroundSize: "20%",
              backgroundPosition: "100% 32px",
            }
      }
    >
      <div className="border-y border-r border-primary w-fit lg:py-4 py-2 lg:pr-3 pr-1 rounded-r-full">
        <div
          className="w-1920:ml-[calc((100vw-1500px)/2)] 2xl:ml-[calc((100vw-1300px)/2)] lg:ml-[calc((100vw-1000px)/2)] px-5 rounded-r-full lg:pr-36 pr-12"
          style={{
            backgroundImage:
              "linear-gradient(-45deg, #F7C1A399 23.31%, #FFFFFF0C 81.45%, #FFFFFF00 84.22%)",
          }}
        >
          <div
            className={`${
              isInView ? "animate__animated animate__fadeInRight" : ""
            }`}
          >
            <div className="text-text-primary font-bold uppercase text-px20 lg:text-px48">
              DỰ ÁN
            </div>
          </div>
        </div>
      </div>
      <div className="lg:pt-16 pt-9">
        <div className="hidden lg:block mx-auto w-1920:w-[1500px] 2xl:w-[1300px] lg:w-[1000px] px-5">
          <div className="relative">
            <div className="w-3/4">
              <img
                src={`${projectListActive[0]?.avatarUrl}`}
                className="w-full aspect-video rounded-[20px]"
                alt={projectListActive[0]?.avatarPath ?? ""}
              />
            </div>
            <div className="absolute lg:top-2/3 right-0 bg-[#EE7633CC] py-8 flex items-center justify-center min-w-[40%] rounded-[10px]">
              <p className="lg:text-px32 text-px16 font-bold text-center text-white line-clamp-2 uppercase">
                {projectListActive[0]?.titleVi}
              </p>
            </div>
          </div>

          <div className="grid grid-cols-[2fr_3fr] gap-10 pt-10">
            <div className="relative">
              <div className="h-full">
                <img
                  src={`${projectListActive[1]?.avatarUrl}`}
                  className="w-full h-full object-cover rounded-[20px]"
                  alt={projectListActive[1]?.avatarPath ?? ""}
                />
              </div>
              <div className="absolute bottom-0 left-0 right-0 bg-[#EE7633CC] h-32 px-5 flex items-center justify-center rounded-[20px]">
                <p className="lg:text-px32 text-px16 font-bold text-center text-white line-clamp-2 uppercase">
                  {projectListActive[1]?.titleVi}
                </p>
              </div>
            </div>
            <div className="relative">
              <div className="h-full">
                <img
                  src={`${projectListActive[2]?.avatarUrl}`}
                  className="w-full aspect-video object-cover rounded-[20px]"
                  alt={projectListActive[2]?.avatarPath ?? ""}
                />
              </div>
              <div className="absolute bottom-0 left-0 right-0 bg-[#EE7633CC] h-32 px-5 flex items-center justify-center rounded-[20px]">
                <p className="lg:text-px32 text-px16 font-bold text-center text-white line-clamp-2 uppercase">
                  {projectListActive[2]?.titleVi}
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="block lg:hidden mx-auto w-1920:w-[1500px] 2xl:w-[1300px] lg:w-[1000px] px-5">
          <div className="grid grid-cols-1 gap-3">
            {[0, 1, 2].map((item, idx) => {
              return (
                <div className="relative" key={idx}>
                  <div className="h-full">
                    <img
                      src={`${projectListActive[item]?.avatarUrl}`}
                      className="w-full aspect-video object-cover rounded-[10px]"
                      alt={projectListActive[item]?.avatarPath ?? ""}
                    />
                  </div>
                  <div className="absolute bottom-0 left-0 right-0 bg-[#EE7633CC] py-5 px-5 flex items-center justify-center rounded-[10px]">
                    <p className="lg:text-px32 text-px16 font-bold text-center text-white line-clamp-2">
                      {projectListActive[item]?.titleVi}
                    </p>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
      {/* <div className="px-[24px] md:px-[50px]  lg:px-[164px]">
        <div className="relative">
          <Swiper
            slidesPerView={2}
            spaceBetween={30}
            centeredSlides={true}
            loop={true}
            // loopFillGroupWithBlank={true}
            autoplay={{
              delay: 2500,
              disableOnInteraction: false,
            }}
            navigation={{
              // Both prevEl & nextEl are null at render so this does not work
              prevEl: navigationPrevRef.current,
              nextEl: navigationNextRef.current,
            }}
            modules={[Navigation, Autoplay]}
            className="h-auto"
          >
            {projectListActive.map((item, index) => {
              return (
                <SwiperSlide className="!w-1/2" key={item.id}>
                  <div>
                    <img
                      src={`${item?.avatarUrl}`}
                      className="w-full aspect-video"
                      alt={item?.avatarPath ?? ""}
                    />
                  </div>
                  <div className="">
                    <p className="text-text-blue text-[20px] font-semibold text-center  sc>768:text-px16 line-clamp-2  w-full mt-[24px]">
                      {item.titleVi}
                    </p>
                  </div>
                </SwiperSlide>
              );
            })}
          </Swiper>
          <div
            ref={navigationPrevRef}
            className="absolute top-[50%] left-[-20px] md:left-[-50px] lg:left-[-60px] cursor-pointer translate-y-[-50%] text-text-primary sc>768:text-[24px] md:text-[48px]"
          >
            <img
              className="w-[10px] h-[20px] lg:w-[23px] lg:h-[49px]"
              src="/images/arrowLeft_icon.svg"
              alt=""
            />
          </div>
          <div
            ref={navigationNextRef}
            className="absolute top-[50%] lssm:right-[-20px] md:right-[-50px] lg:right-[-60px]  cursor-pointer translate-y-[-50%] text-text-primary sc>768:text-[24px] md:text-[48px]"
          >
            <img
              className="w-[10px] h-[20px] lg:w-[23px] lg:h-[49px]"
              src="/images/arrowRight_icon.svg"
              alt=""
            />
          </div>
        </div>
      </div> */}
    </div>
  );
}
