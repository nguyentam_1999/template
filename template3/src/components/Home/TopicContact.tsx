import clsx from "clsx";

import Button from "../../components/Button";
import useInView from "../../hook/useInView";
import useViewport from "../../hook/useViewPort";

export default function TopicContact() {
  const { ref, isInView } = useInView();
  const { width } = useViewport();

  return (
    <div
      className="min-h-[600px] lg:min-h-0"
      ref={ref}
      style={
        width >= 1024
          ? {
              backgroundImage:
                "linear-gradient(174.86deg, #FF6F07 100%, #FFFFFF 0%), url(/images/pictures/bg-contact.png),url(/images/icons/bg-dotted2.png)",
              backgroundSize:
                "70% calc(100% - 100px), 60% calc(100% - 100px), auto 100px",
              backgroundPosition: "0 0, 100% 100%, 25% 100%",
              backgroundRepeat: "no-repeat",
              backgroundBlendMode: "multiply",
            }
          : {
              backgroundImage:
                "linear-gradient(174.86deg, #FF6F07 100%, #FFFFFF 0%), url(/images/pictures/bg-contact.png),url(/images/icons/bg-dotted2.png)",
              backgroundSize: "100% 400px, 70% 40%, 35% auto",
              backgroundPosition: "0 0, 20px 80%, calc(100% - 20px) 95%",
              backgroundRepeat: "no-repeat",
              backgroundBlendMode: "multiply",
            }
      }
    >
      <div className="mx-auto w-1920:w-[1500px] 2xl:w-[1300px] lg:w-[1000px] w-full lg:py-44 py-9 lg:px-5 px-0">
        <div className="grid lg:grid-cols-[1fr_2fr] grid-cols-1 gap-5">
          <div>
            <div>
              <h2
                className={clsx(
                  "lg:pt-5 pt-2 px-5 lg:px-0 lg:text-px32 text-px20 uppercase font-bold text-white ",
                  {
                    "animate__animated animate__fadeInDown": isInView,
                  }
                )}
              >
                LIÊN HỆ VỚI CHÚNG TÔI ĐỂ ĐƯỢC HỖ TRỢ NHANH CHÓNG NHẤT
              </h2>
              <p
                className={clsx(
                  "text-px14 md:text-px18 text-justify text-white lg:px-0 px-5 lg:py-10 py-3",
                  { "animate__animated animate__fadeInLeftBig": isInView }
                )}
              >
                Mong muốn mang đến giá trị to lớn cho đối tác, SME có đội ngũ
                nhiệt tình, chu đáo luôn sẵn sàng giải đáp những thắc mắc, đưa
                ra những giải pháp, thiết kế có thể làm hài lòng những khách
                hàng khó tính nhất.
              </p>
            </div>
            <div
              className={`px-5 lg:px-0 lg:py-8 py-4 flex ${
                isInView ? "animate__animated animate__fadeInTopRight" : ""
              }`}
            >
              <button className="text-px14 lg:text-px18 px-3 lg:px-[34px] lg:py-[14px] py-2 w-fit bg-[#0E2C99] text-white">
                Liên hệ
              </button>
              <div className="bg-white flex justify-center items-center px-2">
                <img className="lg:w-6 w-3" src="/images/arrow.svg" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
