import clsx from "clsx";
import { useEffect, useState } from "react";
import useInView from "../../hook/useInView";
import Button from "../Button";
import Title from "./Title";

export default function TopicAction() {
  const { ref, isInView } = useInView();
  const [categories, setCategories] = useState<any[]>([]);

  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    const res = await (await fetch("api/categories")).json();
    setCategories(res.list);
  };

  return (
    <div ref={ref}>
      <div className="grid lg:grid-cols-[7fr_3fr] grid-cols-1">
        <div className="h-full bg-primary lg:py-24 py-8">
          <div className="border-y border-r border-white w-fit lg:py-4 py-2 lg:pr-3 pr-1 rounded-r-full">
            <div
              className="w-1920:ml-[calc((100vw-1500px)/2)] 2xl:ml-[calc((100vw-1300px)/2)] lg:ml-[calc((100vw-1000px)/2)] px-5 rounded-r-full"
              style={{
                backgroundImage:
                  "linear-gradient(-45deg, #F7C1A399 23.31%, #FFFFFF0C 81.45%, #FFFFFF00 84.22%)",
              }}
            >
              <div
                className={`text-white font-bold uppercase text-px20 lg:text-px40 2xl:text-px48 lg:pr-5 pr-2 ${
                  isInView ? "animate__animated animate__fadeInDown" : ""
                }`}
              >
                LĨNH VỰC HOẠT ĐỘNG
              </div>
            </div>
          </div>
          <div className="w-1920:ml-[calc((100vw-1500px)/2)] 2xl:ml-[calc((100vw-1300px)/2)] lg:ml-[calc((100vw-1000px)/2)] px-5">
            <div className="">
              <h4 className="font-semibold lg:text-px32 text-px16 lg:pt-16 py-10 text-white-color">
                Chế tạo & cung cấp thiết bị ngành
              </h4>
              <div className="grid lg:grid-cols-3 grid-cols-2 lg:gap-12 gap-4">
                {categories.map((item) => {
                  return (
                    <div
                      key={item.id}
                      className="flex flex-col items-center p-2 rounded-[10px] border border-white"
                    >
                      <div
                        className={clsx(
                          "w-full aspect-square rounded-[10px] flex items-center justify-center cursor-pointer lg:px-6 px-3 py-2",
                          {
                            "animate__animated animate__fadeInDown": isInView,
                          }
                        )}
                      >
                        <div className="h-full border-x border-t border-white rounded-t-full p-2">
                          <div
                            className="rounded-t-full flex items-center justify-center h-full"
                            style={{
                              backgroundImage:
                                "linear-gradient(180deg, #FFFFFF88 23.31%, rgba(255, 255, 255, 0.0455) 81.46%, rgba(255, 255, 255, 0) 84.23%)",
                            }}
                          >
                            <img src={item.imageUrl} alt="" />
                          </div>
                        </div>
                      </div>
                      <p
                        className={clsx(
                          "flex-1 lg:text-px20 text-px14 text-center line-clamp-2 text-white-color pt-4 cursor-pointer",
                          { "animate__animated animate__flash": isInView }
                        )}
                      >
                        {item.nameVi}
                      </p>
                      <div>
                        <img
                          className="lg:w-4 w-3 py-2"
                          src="/images/icons/plus-icon.svg"
                          alt=""
                        />
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>
        </div>
        <div>
          <img
            className="h-full object-cover"
            src="/images/pictures/product-pic.png"
            alt=""
          />
        </div>
      </div>
    </div>
  );
}
