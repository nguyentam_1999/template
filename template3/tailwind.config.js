/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        primary: "#FF6F07",
        'text-primary': "#125C92",
        'text-secondary': "#003A3A",
        "secondary-color": "#04DDDD",

        'white-color': "#fff",
        'black-color': "#000",
        "text-blue": "#144E8C",
        "text-title": "#11213F",
      },
      fontSize: {
        px12: "12px",
        px13: "13px",
        px14: "14px",
        px15: "15px",
        px16: "16px",
        px17: "17px",
        px18: "18px",
        px20: "20px",
        px24: "24px",
        px28: "28px",
        px32: "32px",
        px40: "40px",
        px48: "48px",
        px64: "64px"
      }

    },
    screens: {
      "sc>768": { max: "767px" },
      "sc991": { max: '991px' },
      "sc<992": { min: '992px' },
      'lssm': '280px',
      'sm-390': '390px',
      'lsm-320': "320px",
      'lsm-380': '380px',
      'sm-480': "480px",
      'sm': '640px',
      // => @media (min-width: 640px) { ... }

      'md': '768px',
      // => @media (min-width: 768px) { ... }
      'm992': '992px',
      // => @media (min-width: 768px) { ... }

      'lg': '1024px',
      // => @media (min-width: 1024px) { ... }

      'xl': '1280px',
      // => @media (min-width: 1280px) { ... }

      '2xl': '1536px',
      // => @media (min-width: 1536px) { ... }
      'w-1920': '1920px',
      // => @media (min-width: 1920px) { ... }
      "sc2200": { min: '2250px' }
    }
  },
  plugins: [
    require('@tailwindcss/line-clamp'),
  ]
  // important: true
}
