import { useEffect } from "react";
import Button from "../../components/Button";
import SliderBannerHome from "./SliderBanner";
export default function Banner() {


  return (
    <div className="flex relative items-center justify-center text-center banner_home_primary relative text-white-color">
      <div className="h-full  absolute inset-0 z-[2] mix-blend-multiply w-full  bg-[#5b929966]" />
      <div className="flex flex-col items-center justify-end pb-[115px] h-full my-auto  sc991:absolute z-[2] lssm:px-[24px] sm:px-[40px] md:px-[80px] lg:px-[120px] 2xl:px-[242px] w-full">
        <p className="text-[20px] font-bold uppercase lg:text-[48px]">XIN CHÀO!</p>
        <div className="lssm:w-fit xl:w-max text-[32px] font-bold uppercase lg:text-[70px]  2xl:text-[110px]  relative z-[1]  animate__animated animate__fadeInDown">
            WE ARE POLYCO
        </div>
        <p className="text-px14 lg:text-px20 sc>768:px[24px] lssm:mb-[14px] md:mb-[28px] lssm:leading-4 md:leading-7 sc991:text-white-color animate__animated animate__fadeInUp">
         Chúng tôi luôn cung cấp cho khách hàng những sản phẩm và dịch vụ tốt nhất.
        </p>
        <Button
          color="primary"
          className="text-px14 lg:text-px18 px-[34px] py-[14px] max-w-fit"
        >
            Liên hệ hợp tác
        </Button>
      </div>
      <div className="w-full max-w-full min-h-[100%] inset-0 absolute overflow-hidden banner_home-after">
        <SliderBannerHome />
      </div>
    </div>
  );
}
