import useInView from '../../hook/useInView';
import Title from './Title'
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
// import "swiper/modules/grid";
import "swiper/css/navigation";
import { Autoplay, Navigation, Grid } from "swiper";
import React, { useMemo, useState, useEffect } from "react";
import clsx from 'clsx';

const TopicCustomer = () => {

    const { isInView, ref } = useInView();
    const [partners, setPartners] = useState<any[]>([])

    const width = useMemo(() => {
        return window.innerWidth;
    }, []);

    useEffect(() => {
        getData();
    }, [])

    const getData = async () => {
        const res = await (await fetch("api/partner")).json();
        setPartners(res);
    }

    const navigationPrevRef = React.useRef(null);
    const navigationNextRef = React.useRef(null);

    return (
        <div className="h-max sc>768:pb-[30px] mt-[20px] lg:mt-[100px]  flex flex-col" ref={ref}>
            <div className="pl-[24px] md:pl-[80px] xl:pl-[50px]  w-1920:pl-[216px]">
                <div
                    className={clsx(
                        " text-[#2B6FA6] font-bold uppercase 2xl:leading-[35px]  text-px20 md:text-[32px] flex items-center xl:text-[34px] 2xl:text-[48px]",
                        { "animate__animated animate__fadeInRight": isInView }
                    )}
                >
                    <svg width={width > 1024 ? "88" : "35"} height={width > 1024 ? "66" : "25"} viewBox="0 0 88 66" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M77.3436 0.60791H47.3804L59.3657 34.4489L47.3804 65.0886H77.3436L87.5664 34.4489L77.3436 0.60791Z" fill="#F60000" />
                        <path d="M30.1718 0.60791H0.208485L12.1938 34.4489L0.208485 65.0886H30.1718L40.3945 34.4489L30.1718 0.60791Z" fill="#F60000" />
                    </svg>

                    <span className="ml-[7px] lg:ml-[31px]">KHÁCH HÀNG</span>
                    <div className="flex-1 flex flex-col items-end h-[120%] justify-between  ml-[40px]">
                        <div className="w-[30%] lg:w-[20%] h-[3px] lg:h-[10px]  bg-primary"></div>
                        <div className="w-full h-[1px] lg:h-[6px] mt-[3px] lg:mt-[22px] bg-primary"></div>
                    </div>
                </div>

            </div>
            <div className='mt-[18px] lg:mt-[70px] h-[153px] md:h-[507px] relative'>
                <div className="relative mt-[50px] md:mt-[127px] h-fit lssm:px-[24px] md:px-[80px]  xl:px-[50px]  w-1920:px-[216px]">
                    <Swiper
                        slidesPerView={3
                        }
                        spaceBetween={30}
                        // slidesPerGroup={1}
                        loop={false}
                        loopFillGroupWithBlank={true}
                        autoplay={{
                            delay: 2500,
                            disableOnInteraction: false,
                        }}
                        navigation={{
                            // Both prevEl & nextEl are null at render so this does not work
                            prevEl: navigationPrevRef.current,
                            nextEl: navigationNextRef.current,
                        }}
                        onSwiper={(swiper: any) => {
                            // Delay execution for the refs to be defined
                            setTimeout(() => {
                                // Override prevEl & nextEl now that refs are defined
                                swiper.params.navigation.prevEl = navigationPrevRef.current;
                                swiper.params.navigation.nextEl = navigationNextRef.current;

                                // Re-init navigation
                                swiper.navigation.destroy();
                                swiper.navigation.init();
                                swiper.navigation.update();
                            });
                        }}
                        modules={[Navigation, Autoplay]}
                        className="slider-company-home bg-transparent"
                    >
                        {partners.map((item, index) => {
                            if ((index + 1) % 2 === 0 && width < 768) return "";
                            return (
                                <SwiperSlide className="slider-company-home-item " key={item.id}>
                                    <div className="h-[55px] xl:h-[241px]">
                                        <img
                                            src={item?.imageUrl ?? ""}
                                            className="max-h-[70%]  object-contain w-[70%] h-full"
                                            alt={item?.imageUrl ?? ""}
                                        />
                                    </div>
                                </SwiperSlide>
                            )

                        })}
                    </Swiper>

                    <div ref={navigationPrevRef} className="absolute top-[50%] translate-y-[-50%] bg-transparent left-[5px] md:left-[5px] w-1920:left-[80px] cursor-pointer text-[#fff] sc>768:text-[24px] md:text-[48px]">
                        <svg width={width > 767 ? "20" : "10"} height={width > 767 ? "40" : "30"} viewBox="0 0 10 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M8.71065 0.237078L0.23225 9.39693C0.0833396 9.55782 0 9.7736 0 9.99828C0 10.223 0.0833396 10.4387 0.23225 10.5996L8.71065 19.7629C8.78001 19.838 8.86295 19.8976 8.95456 19.9383C9.04616 19.979 9.14459 20 9.24402 20C9.34346 20 9.44188 19.979 9.53349 19.9383C9.6251 19.8976 9.70803 19.838 9.7774 19.7629C9.92011 19.6091 10 19.4025 10 19.1874C10 18.9723 9.92011 18.7658 9.7774 18.6119L1.80457 9.99828L9.7774 1.38637C9.91962 1.23258 9.9992 1.02639 9.9992 0.811722C9.9992 0.597052 9.91962 0.390865 9.7774 0.237078C9.70803 0.162049 9.6251 0.102416 9.53349 0.061697C9.44188 0.0209784 9.34346 0 9.24402 0C9.14459 0 9.04616 0.0209784 8.95456 0.061697C8.86295 0.102416 8.78001 0.162049 8.71065 0.237078Z" fill="#fff" />
                        </svg>
                    </div>
                    <div ref={navigationNextRef} className="absolute top-[50%] translate-y-[-50%] right-[5px] md:right-[5px] w-1920:right-[80px]  cursor-pointer text-[#fff] sc>768:text-[24px] md:text-[48px]">
                        <svg width={width > 767 ? "20" : "10"} height={width > 767 ? "40" : "30"} viewBox="0 0 10 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M1.28935 0.237078L9.76775 9.39693C9.91666 9.55782 10 9.7736 10 9.99828C10 10.223 9.91666 10.4387 9.76775 10.5996L1.28935 19.7629C1.21999 19.838 1.13705 19.8976 1.04544 19.9383C0.953836 19.979 0.855411 20 0.755977 20C0.656544 20 0.558116 19.979 0.466508 19.9383C0.374899 19.8976 0.291965 19.838 0.2226 19.7629C0.0798931 19.6091 0 19.4025 0 19.1874C0 18.9723 0.0798931 18.7658 0.2226 18.6119L8.19543 9.99828L0.2226 1.38637C0.0803833 1.23258 0.000799179 1.02639 0.000799179 0.811722C0.000799179 0.597052 0.0803833 0.390865 0.2226 0.237078C0.291965 0.162049 0.374899 0.102416 0.466508 0.061697C0.558116 0.0209784 0.656544 0 0.755977 0C0.855411 0 0.953836 0.0209784 1.04544 0.061697C1.13705 0.102416 1.21999 0.162049 1.28935 0.237078Z" fill="#fff" />
                        </svg>


                    </div>
                </div>
                <img className='absolute inset-0 w-full h-full z-[-1]' src='/images/contact_bg.png' alt='' />
            </div>

        </div>
    )
}

export default TopicCustomer