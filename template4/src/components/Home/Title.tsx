import React from 'react'
import clsx from "clsx";

type Props = {
    title: string,
    isInView: boolean
}

const Title = ({ title, isInView }: Props) => {
    return (
        <div className="pr-0 lssm:px-[24px] md:px-[80px] mb-[28px] lg:mb-[60px]  xl:px-[50px]  w-1920:px-[202px]">
            <div
                className={clsx(
                    " text-text-primary font-bold uppercase 2xl:leading-[35px]  text-px20 md:text-[32px] flex items-center xl:text-[34px] 2xl:text-[48px]",
                    { "animate__animated animate__fadeInRight": isInView }
                )}
            >
                <img src="/images/icon_title.svg" className="w-[35px] h-[25px] lg:h-auto lg:w-auto  mr-[8px] lg:mr-[31px]" /> {title}
            </div>

        </div>
    )

}

export default Title