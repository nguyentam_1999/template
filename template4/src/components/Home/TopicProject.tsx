import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
// import "swiper/modules/grid";
import "swiper/css/navigation";
import { Autoplay, Navigation, } from "swiper";
import React, { useMemo, useState, useEffect } from "react";
import Title from "./Title";
import useInView from "../../hook/useInView";
import clsx from "clsx";
const LIMIT = 6;

export default function SliderProject() {


    const [projectListActive, setProject] = useState<any[]>([])
    const { isInView, ref } = useInView();
    const navigationPrevRef = React.useRef(null)
    const navigationNextRef = React.useRef(null)

    const width = useMemo(() => {
        return window.innerWidth;
    }, []);

    useEffect(() => {
        getData()
    }, [])


    const getData = async () => {
        const res = await (await fetch("api/project/priority")).json()

        setProject(res);
    }



    return (
        <div ref={ref} className="mt-[40px] lg:mt-[128px]  pl-[24px] md:pl-[80px] xl:pl-[50px]    w-1920:pl-[216px]">
            <div className="">
                <div
                    className={clsx(
                        " text-[#2B6FA6] font-bold uppercase 2xl:leading-[35px]  text-px20 md:text-[32px] flex items-center xl:text-[34px] 2xl:text-[48px]",
                        { "animate__animated animate__fadeInRight": isInView }
                    )}
                >
                    <svg width={width > 1024 ? "88" : "35"} height={width > 1024 ? "66" : "25"} viewBox="0 0 88 66" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M77.3436 0.60791H47.3804L59.3657 34.4489L47.3804 65.0886H77.3436L87.5664 34.4489L77.3436 0.60791Z" fill="#F60000" />
                        <path d="M30.1718 0.60791H0.208485L12.1938 34.4489L0.208485 65.0886H30.1718L40.3945 34.4489L30.1718 0.60791Z" fill="#F60000" />
                    </svg>

                    <span className="ml-[7px] lg:ml-[31px]">Dự án</span>
                    <div className="flex-1 flex flex-col items-end h-[120%] justify-between  ml-[40px]">
                    <div className="w-[30%] lg:w-[20%] h-[3px] lg:h-[10px]  bg-primary"></div>
                        <div className="w-full h-[1px] lg:h-[6px] mt-[3px] lg:mt-[22px] bg-primary"></div>
                    </div>
                </div>

            </div>
            <div className="mt-[78px] lg:mt-[96px] pr-[24px] md:pr-[80px] xl:pr-[50px] w-1920:pr-[216px]">
                <div className="relative">
                    <div>
                        <div className="flex">
                            <div className="w-full  rounded-[10px] lg:w-[70%] relative">
                                <img className="w-full min-h-[404px] rounded-[10px] object-cover" src={projectListActive[0]?.avatarUrl} alt="" />
                                <div className="rounded-[10px] absolute bottom-0 lg:bottom-[145px]  text-[16px]  lg:text-[32px] font-semibold flex items-center justify-center text-[#fff] px-[34px] bg-[#f60000cc] h-[55px] lg:h-[114px] w-full lg:w-auto  right-0 lg:translate-x-[50%]">
                                    {projectListActive[0]?.titleVi}
                                </div>
                            </div>
                            <img src="/images/project_dot.png" alt="" className="ml-auto hidden lg:block h-fit" />
                        </div>
                        <div className="flex flex-col lg:flex-row mt-[12px] lg:mt-[43px]">
                            {
                                projectListActive.length > 2 && (
                                    <div className="lg:w-[40%] relative  rounded-[20px] overflow-hidden h-[404px] lg:mr-[35px]">
                                        <img className="w-full h-full object-cover " src={projectListActive[1].avatarUrl} alt="" />
                                        <div className="text-center justify-center h-[78px] lg:h-[146px] flex items-center px-[50px] 2xL:px-[133px] absolute text-[16px] lg:text-[32px] text-[#fff] bottom-0 bg-[#f60000cc] w-full">
                                            {projectListActive[1]?.titleVi}
                                        </div>
                                    </div>

                                )
                            }

{
                                projectListActive.length > 3 && (
                                    <div className="lg:w-[60%] mt-[12px] lg:mt-0 relative  rounded-[20px] overflow-hidden h-[404px]">
                                        <img className="w-full h-full object-cover " src={projectListActive[2].avatarUrl} alt="" />
                                        <div className="text-center justify-center  h-[78px] lg:h-[146px] flex items-center px-[50px] 2xL:px-[133px] absolute text-[16px] lg:text-[32px] text-[#fff] bottom-0 bg-[#f60000cc] w-full">
                                        {projectListActive[2]?.titleVi}
                                        </div>
                                    </div>

                                )
                            }

                        </div>
                    </div>
                </div>

            </div>
        </div>
    );
}


