import clsx from "clsx";
import { useEffect, useMemo, useState } from "react";
import useInView from "../../hook/useInView";
import Button from "../Button";
import Title from "./Title";

export default function TopicAction() {
  const { ref, isInView } = useInView();
  const [categories, setCategories] = useState<any[]>([]);


  useEffect(() => {
    getData();
  }, [])


  const getData = async () => {
    const res = await (await fetch("api/categories")).json();
    setCategories(res.list)
  }

  const width = useMemo(() => {
    return window.innerWidth;
  }, []);




  return (
    <div className="py-[45px] xl:py-[126px] bg-[#4F9DD6] pl-[24px] md:pl-[80px] xl:pl-[50px]    w-1920:pl-[216px]">
      <div className="h-max pr-[24px] md:pr-[80px] lg:pr-0">
        <div
          className={clsx(
            " text-[#F2F5EF] font-bold uppercase 2xl:leading-[35px]  text-px20 lg:text-[32px] flex-col flex lg:flex-row lg:items-center xl:text-[34px] 2xl:text-[48px]",
            { "animate__animated animate__fadeInRight": isInView }
          )}
        >
          <div className="flex items-center">
            <svg width={width > 1024 ? "88" : "35"} height={width > 1024 ? "66" : "25"} viewBox="0 0 88 66" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M77.3436 0.60791H47.3804L59.3657 34.4489L47.3804 65.0886H77.3436L87.5664 34.4489L77.3436 0.60791Z" fill="#FFFFFF" />
              <path d="M30.1718 0.60791H0.208485L12.1938 34.4489L0.208485 65.0886H30.1718L40.3945 34.4489L30.1718 0.60791Z" fill="#FFFFFF" />
            </svg>

            <span className="ml-[7px] lg:ml-[31px]">LĨNH VỰC HOẠT ĐỘNG</span>

          </div>
          <div className="lg:flex-1 flex flex-col items-end justify-between my-[46px]  lg:ml-[40px]">
            <div className="w-[30%] lg:w-[20%] h-[4px] lg:h-[10px]  bg-white"></div>
            <div className="w-[50%] lg:w-full h-[4px] lg:h-[6px] mt-[5px] lg:mt-[22px] bg-white "></div>
          </div>
        </div>

      </div>

      <div
        className="h-full lssm:pr-[24px] md:pr-[80px]  xl:pr-[50px]  w-1920:pr-[216px]"
        ref={ref}
      >
        <div className="flex flex-col lg:flex-row w-full h-full ">
          <div className=" xl:mt-[24px] 2xl:mt-[62px] flex flex-col flex-1  h-auto">
            <div className="">
              <h4 className="font-semibold text-center lg:text-left text-px16 lg:text-[32px] text-white-color lssm:mb-[24px] lg:mb-[47px]">
                Chế tạo & cung cấp thiết bị ngành
              </h4>
              <div className="grid grid-cols-2  sm-480:grid-cols-3 lg:grid-cols-3 2xl:grid-cols-5 gap-x-[12px] gap-y-[12px] lsm-320:gap-x-[25px] lsm-320:gap-y-[22px] lg:gap-x-[46px] lg:gap-y-[32px]">
                {categories.map((item) => {
                  return (
                    <div key={item.id} className="">
                      <div
                        className={clsx(
                          "min-w-[100%] w-[133px]  h-[137px] lg:w-[237px] max-w-full lg:h-[244px] rounded-[10px] border-2 border-solid border-white-color flex items-center justify-center cursor-pointer",
                          { "animate__animated animate__fadeInDown": isInView }
                        )}
                      >
                        <img className="w-[100%] h-[100%] object-cover" src={item.imageUrl} alt="" />
                      </div>
                      <p
                        className={clsx(
                          "text-px14  lg:text-px20 line-clamp-2 text-center  text-white-color mt-[14px] cursor-pointer",
                          { "animate__animated animate__flash": isInView }
                        )}
                      >
                        {item.nameVi}
                      </p>
                    </div>
                  );
                })}
              </div>

            </div>

          </div>
        </div>
      </div>
    </div>
  );
}
