import clsx from "clsx";

import Button from "../../components/Button";
import useInView from "../../hook/useInView";


export default function TopicContact() {

    const { ref, isInView } = useInView()

    return (
        <div className="h-auto  pb-[149px] max-w-full lg:pr-[24px] 2xl:pr-[85px]  relative " ref={ref}>
            <div className="relative">
                <div className="h-[363px] lg:h-[500px] 2xl:h-[794px]  bg-[#4F9DD6] lg:w-[80%]  lg:flex-row lg:items-center">
                    <div className="w-[100%] lg:w-[60%] 2xl:w-[40%] h-full flex flex-col py-[47px] lg:py-[67px] justify-start lg:justify-center lssm:px-[24px] md:pl-[80px] lg:pr-[70px]  xl:pl-[50px]  w-1920:pl-[216px] text-text-title">
                        {/* <div className="mb-[29px]">
                        <img src="/images/icon_contact.svg" alt="" />
                    </div> */}
                        <h2 className={clsx("text-[#fff] text-[20px] lg:text-[32px] uppercase font-bold", { "animate__animated animate__fadeInDown": isInView })}>LIÊN HỆ VỚI CHÚNG TÔI ĐỂ ĐƯỢC HỖ TRỢ NHANH CHÓNG NHẤT</h2>
                        {/* <div className="sc991:flex  justify-between  mt-[15px] lg:mt-[24px] ">
                        <div className="w-[330px] h-[2px] bg-[#144E8C]" />
                    </div> */}
                        <p className={clsx("text-[14px] md:text-px18 text-justify mt-[15px] lg:mt-[48px]  text-[#fff]", { "animate__animated animate__fadeInLeftBig": isInView })}>Mong muốn mang đến giá trị to lớn cho đối tác, SME có đội ngũ nhiệt tình, chu đáo luôn sẵn sàng giải đáp những thắc mắc, đưa ra những giải pháp, thiết kế có thể làm hài lòng những khách hàng khó tính nhất.</p>
                        <div className={clsx("w-full mt-[30px] lg:mt-[37px]", { "animate__animated animate__fadeInTopRight": isInView })}>
                            <div
                                className="text-[14px] lg:text-[18px] text-[#fff] flex items-center h-[33px]  lg:h-[50px] w-[144px] lg:w-[202px] bg-primary shadow-md"
                            >
                                <span className="flex items-center justify-center  text-inherit flex-1 font-bold">
                                    Liên hệ
                                </span>
                                <div className="h-full flex items-center px-[8px] bg-white">
                                    <img src="/images/arrow_left.png" alt="" />
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div className="absolute bottom-[-110px] lg:bottom-[-150px] right-[80px] lg:right-0">
                    <img className="w-[268px] h-[180px] lg:w-[500px] 2xl:w-auto lg:h-[500px] 2xl:h-[749px] object-cover" src="/images/contact_form.png" alt="" />
                </div>
            </div>
                <div className="absolute bottom-[11px] z-[-1] right-0 lg:right-[unset] lg:left-[150px] xl:left-[250px] w-1920:left-[450px]">
                    <img src="/images/contact_dots.png" className="hidden lg:block" alt="" />
                    <img src="/images/contact_dots_mobile.png" className="block lg:hidden" alt="" />
                </div>

        </div>
    )

}