import clsx from "clsx";
import useInView from "../../hook/useInView";
import Button from "../Button";
import { useEffect, useMemo, useRef } from "react";
import Title from "./Title";

export default function TopicSuccess() {
  const { ref, isInView } = useInView();
  const refFrame = useRef<HTMLIFrameElement>(null);

  useEffect(() => {
    if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
      create_iframe();
    }
  }, [refFrame.current]);

  function create_iframe() {
    if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
      refFrame.current?.classList.add("ios");
    }
  }

  return (
    <div className="" ref={ref}>
      <div className="py-[24px] xl:py-[90px] xl:pl-[24px] 2xl:pl-[50px]  w-1920:pl-[216px] ">
        <div className="relative flex">
          <div
            className={clsx(
              "container-parallelogram flex items-center px-[24px] xl:px-[90px] 2xl:pl-[80px] 2xl:pr-[100px] w-1920:pl-[110px] w-1920:pr-[200px] font-bold justify-center text-[20px] 2xl:text-[48px] xl:text-[40px] text-white w-[90%] md:w-[87%] xl:w-[61%] 2xl:w-[60%]",
              { "animate__animated animate__fadeInRight": isInView }
            )}
          >
            THÀNH CÔNG CỦA BẠN LÀ ĐỊNH HƯỚNG CỦA CHÚNG TÔI
          </div>

          <div className="absolute right-0 bottom-0 w-[14%] xl:w-[43%] h-[20px] bg-primary cut-bg"></div>
        </div>
      </div>

      <div className="relative">
        <div
          className={clsx(
            "grid grid-cols-1 xl:grid-cols-3 lg:flex-row h-max pl-[-20px] lssm:pl-0 2xl:pl-[80px]  xl:pl-[50px] xl:gap-x-[30px]  w-1920:pl-[216px] pt-[37px] xl:py-[100px]"
          )}
        >
          <div
            ref={refFrame}
            className={clsx("relative sc991:w-full px-[50px] xl:px-0", {
              "animate__animated animate__fadeInLeft": isInView,
            })}
          >
            <div className="z-[-1] absolute top-0 left-0  rounded-tl-[10px] lg:rounded-tl-[30px] h-[50%]"></div>
            <div>
              <iframe
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                scrolling="no"
                allowFullScreen={true}
                className="sc991:w-full h-[179px]  md:h-[320px] lg:h-[340px] xl:h-[400px] md:w-full object-cover rounded-[10px]"
                width="100%"
                height="315"
                src={`https://www.youtube.com/embed/${
                  true ? "0sIktMTze1Y" : "aUatyOjy3A8"
                }`}
                title="Anfico"
              ></iframe>
              <img
                src="/images/dot_desktop.png"
                alt=""
                className="ml-auto mt-[24px] hidden xl:block"
              />
            </div>
          </div>
          <div
            className={clsx(
              "col-span-1 xl:col-span-2 flex flex-col justify-between text-white bg-[#2b6fa6d5] py-[25px] xl:py-[50px] px-[24px] 2xl:pl-[90px] 2xl:pr-[190px] h-auto xl:max-h-[400px] sc991:w-full mt-[28px] xl:mt-0"
            )}
          >
            <div
              className={clsx("text-px14 lg:text-px16  text-justify", {
                "animate__animated animate__flash": isInView,
              })}
            >
              <div
                className={clsx(
                  "text-px14 lg:text-px18 text-justify text-text-secondary py-2"
                )}
              >
                <p>
                  Với tâm huyết của ban lãnh đạo, kinh nghiệm và quyết tâm của
                  toàn thể đội ngũ kỹ thuật, công nhân viên, chúng tôi luôn tạo
                  ra những sản phẩm có chất lượng, thẩm mỹ cao. Bên cạnh đó
                  không ngừng cập nhật nâng cấp công nghệ, quy trình sản xuất để
                  đáp ứng được mọi yêu cầu của Quý khách hàng gần xa cả về chất
                  lượng và tiến độ.
                </p>
                <p>
                  Công ty chú trọng tuyệt đối đến chất lượng sản phẩm thông qua
                  việc lựa chọn nhà cung cấp nguyên liệu uy tín, đầu tư trang
                  thiết bị máy móc hiện đại cùng với những kỹ sư giàu kinh
                  nghiệm, đội ngũ công nhân lành nghề và hệ thống quản lý khoa
                  học. Chúng tôi tự tin khẳng định sản phẩm của mình có thể sánh
                  ngang với các sản phẩm thuộc phân khúc cao cấp đến từ châu Âu.
                </p>
              </div>
            </div>
            <div className="flex justify-between mt-[28px] xl:mt-[30px]">
              <div className="text-px14 lg:text-[18px]  flex items-center h-[33px] lg:h-[50px] w-[144px] lg:w-[202px] bg-primary shadow-md">
                <span className="flex items-center justify-center  text-inherit flex-1 font-bold">
                  Xem thêm
                </span>
                <div className="h-full flex items-center px-[8px] bg-white">
                  <img src="/images/arrow_left.png" alt="" />
                </div>
              </div>
              <div>
                <img
                  className="2xl:hidden"
                  src="/images/dot_moblie.png"
                  alt=""
                />
              </div>
            </div>
            <div></div>
          </div>

          <img
            src="/images/success_bg.png"
            alt=""
            className="absolute  inset-0 z-[-1] w-full h-full hidden xl:block"
          />
          <img
            src="/images/bg_success_moblie.png"
            alt=""
            className="absolute  inset-0 z-[-1] w-full  h-auto max-h-full xl:hidden"
          />
        </div>
      </div>
    </div>
  );
}
