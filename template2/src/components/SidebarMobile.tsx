import clsx from "clsx";
import React from "react";
import Button from "./Button";

type Props = {
  isShow: boolean;
  showSidebarModal: () => void;
};

const SidebarMobile = ({ isShow, showSidebarModal }: Props) => {
  return (
    <>
      <div
        className={clsx(
          "bg-white side-bar w-[70%] lsm-320:w-[70%] md:w-[40%] xl:hidden ",
          { "side-bar-show": isShow, "side-bar-hide": !isShow }
        )}
      >
        <div className="py-[24px]">
          <div className="px-[24px] flex items-center">
            <div className="flex-1 mr-[24px]">
              <input
                placeholder="Tìm kiếm"
                className="p-2 w-full h-[40px] rounded-[50px] border border-primary"
              />
            </div>
            <div>
              <div className="">
                <img src="/images/usa_icon.svg" alt="" />
              </div>

              <div className="mt-[12px] w-fit h-fit border border-blue-800 rounded-[4px]">
                <img src="/images/vn_icon.svg" alt="" />
              </div>
            </div>
          </div>
          <div className="px-[24px] py-[24px] border-b border-[#ccc]">
            <Button color="primary">Đăng nhập</Button>
          </div>
          <div className="px-[24px] [&>div]:py-[24px] text-[14px]">
            <div className="flex items-center">
              <div className="mr-[15px]">
                <img src="/images/home_icon.jpg" />
              </div>
              <span className="text-[#0058DB]">Trang chủ</span>
            </div>

            <div className="flex items-center">
              <div className="mr-[15px]">
                <img src="/images/about_icon.jpg" />
              </div>
              <span>Giới thiệu</span>
            </div>

            <div className="flex items-center">
              <div className="mr-[15px]">
                <img src="/images/product_icon.jpg" />
              </div>
              <span>Sản phẩm</span>
            </div>

            <div className="flex items-center">
              <div className="mr-[15px]">
                <img src="/images/product_icon.jpg" />
              </div>
              <span>Dự án</span>
            </div>

            <div className="flex items-center">
              <div className="mr-[15px]">
                <img src="/images/contact_icon.jpg" />
              </div>
              <span>Liên hệ</span>
            </div>

            <div className="flex items-center">
              <div className="mr-[15px]">
                <img src="/images/news_icon.jpg" />
              </div>
              <span>Tin tức</span>
            </div>

            <div className="flex items-center">
              <div className="mr-[15px]">
                <img src="/images/news_icon.jpg" />
              </div>
              <span>Thư viện hình ảnh</span>
            </div>
          </div>
        </div>
      </div>
      <div
        onClick={showSidebarModal}
        className={clsx("modal-sidebar lg:hidden", {
          block: isShow,
          hidden: !isShow,
        })}
      />
    </>
  );
};

export default SidebarMobile;
