import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
// import "swiper/modules/grid";
import "swiper/css/navigation";
import { Autoplay, Navigation, Pagination } from "swiper";
import React, { useMemo, useState, useEffect } from "react";
import Title from "./Title";
import useInView from "../../hook/useInView";
import useViewport from "../../hook/useViewPort";
const LIMIT = 6;

export default function SliderProject() {
  const [projectListActive, setProject] = useState<any[]>([]);
  const { isInView, ref } = useInView();
  const navigationPrevRef = React.useRef(null);
  const navigationNextRef = React.useRef(null);
  const paginationRef = React.useRef(null);

  const { width } = useViewport();

  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    const res = await (await fetch("api/project/priority")).json();

    setProject(res);
  };

  return (
    <div className="lg:pt-32 pt-8" ref={ref}>
      <div className="mx-auto w-1920:w-[1500px] 2xl:w-[1300px] lg:w-[1000px] px-5">
        <div
          className={`flex items-center ${
            isInView ? "animate__animated animate__fadeInRight" : ""
          }`}
        >
          <img
            src="/images/icon_title.svg"
            className="lg:w-20 w-8 2xl:mr-5 mr-2"
          />
          <div className="text-text-primary font-bold uppercase text-px20 lg:text-px48 lg:py-10 py-2">
            DỰ ÁN
          </div>
        </div>
      </div>
      <div className="grid w-1920:grid-cols-[1fr_1500px_1fr] 2xl:grid-cols-[1fr_1300px_1fr] lg:grid-cols-[1fr_1000px_1fr] grid-cols-1">
        <div className="lg:block hidden">
          <div className="w-full h-full flex items-center justify-center">
            <img
              ref={navigationPrevRef}
              className="w-[10px] lg:w-[23px] cursor-pointer"
              src="/images/arrowLeft_icon.svg"
              alt=""
            />
          </div>
        </div>
        <div className="px-5">
          <Swiper
            slidesPerView={width >= 1024 ? 3 : 1}
            spaceBetween={20}
            centeredSlides={true}
            loop={true}
            loopFillGroupWithBlank={true}
            autoplay={{
              delay: 2500,
              disableOnInteraction: false,
            }}
            modules={[Navigation, Autoplay]}
            className="h-auto"
          >
            {projectListActive.map((item, index) => {
              return (
                <SwiperSlide className="" key={item.id}>
                  <div className="">
                    <div className="">
                      <img
                        src={`${item?.avatarUrl}`}
                        className="w-full aspect-video max-h-[500px] rounded-[10px]"
                        alt={item?.avatarPath ?? ""}
                      />
                    </div>
                    <div className="flex items-center justify-center py-5 px-5">
                      <p className="lg:text-px20 text-px16 font-bold text-center text-[#144E8C] line-clamp-2">
                        {item.titleVi}
                      </p>
                    </div>
                  </div>
                </SwiperSlide>
              );
            })}
          </Swiper>
        </div>
        <div className="lg:block hidden">
          <div className="w-full h-full flex items-center justify-center">
            <img
              ref={navigationNextRef}
              className="w-[10px] lg:w-[23px] cursor-pointer"
              src="/images/arrowRight_icon.svg"
              alt=""
            />
          </div>
        </div>
      </div>
      {/* <div className="lg:pt-16 pt-6">
        <Swiper
          slidesPerView={2}
          spaceBetween={0}
          centeredSlides={true}
          loop={true}
          // loopFillGroupWithBlank={true}
          autoplay={{
            delay: 2500,
            disableOnInteraction: false,
          }}
          pagination={{
            el: paginationRef.current,
            renderBullet: function (index, className) {
              return (
                '<span class="!h-1 flex-1 !rounded-none ' +
                className +
                '"></span>'
              );
            },
          }}
          modules={[Navigation, Pagination, Autoplay]}
          className="h-auto"
        >
          {projectListActive.map((item, index) => {
            return (
              <SwiperSlide className="!w-1/2 lg:px-2 px-1" key={item.id}>
                <div className="relative">
                  <div className="">
                    <img
                      src={`${item?.avatarUrl}`}
                      className="w-full aspect-video max-h-[500px]"
                      alt={item?.avatarPath ?? ""}
                    />
                  </div>
                  <div className="absolute lg:top-3/4 bottom-5 left-0 right-0 bg-[#E5CF03CC] h-16 flex items-center justify-center">
                    <p className="lg:text-px20 text-px16 font-bold text-center text-white line-clamp-2 uppercase">
                      {item.titleVi}
                    </p>
                  </div>
                </div>
              </SwiperSlide>
            );
          })}
        </Swiper>
        <div className="lg:py-14 py-4">
          <div
            className="flex justify-center mx-auto lg:!w-[600px] w-full px-5 [&>.swiper-pagination-bullet-active]:!bg-primary"
            ref={paginationRef}
          ></div>
        </div>
      </div> */}
      {/* <div className="px-[24px] md:px-[50px]  lg:px-[164px]">
        <div className="relative">
          <Swiper
            slidesPerView={2}
            spaceBetween={30}
            centeredSlides={true}
            loop={true}
            // loopFillGroupWithBlank={true}
            autoplay={{
              delay: 2500,
              disableOnInteraction: false,
            }}
            navigation={{
              // Both prevEl & nextEl are null at render so this does not work
              prevEl: navigationPrevRef.current,
              nextEl: navigationNextRef.current,
            }}
            modules={[Navigation, Autoplay]}
            className="h-auto"
          >
            {projectListActive.map((item, index) => {
              return (
                <SwiperSlide className="!w-1/2" key={item.id}>
                  <div>
                    <img
                      src={`${item?.avatarUrl}`}
                      className="w-full aspect-video"
                      alt={item?.avatarPath ?? ""}
                    />
                  </div>
                  <div className="">
                    <p className="text-text-blue text-[20px] font-semibold text-center  sc>768:text-px16 line-clamp-2  w-full mt-[24px]">
                      {item.titleVi}
                    </p>
                  </div>
                </SwiperSlide>
              );
            })}
          </Swiper>
          <div
            ref={navigationPrevRef}
            className="absolute top-[50%] left-[-20px] md:left-[-50px] lg:left-[-60px] cursor-pointer translate-y-[-50%] text-text-primary sc>768:text-[24px] md:text-[48px]"
          >
            <img
              className="w-[10px] h-[20px] lg:w-[23px] lg:h-[49px]"
              src="/images/arrowLeft_icon.svg"
              alt=""
            />
          </div>
          <div
            ref={navigationNextRef}
            className="absolute top-[50%] lssm:right-[-20px] md:right-[-50px] lg:right-[-60px]  cursor-pointer translate-y-[-50%] text-text-primary sc>768:text-[24px] md:text-[48px]"
          >
            <img
              className="w-[10px] h-[20px] lg:w-[23px] lg:h-[49px]"
              src="/images/arrowRight_icon.svg"
              alt=""
            />
          </div>
        </div>
      </div> */}
    </div>
  );
}
