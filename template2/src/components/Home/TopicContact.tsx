import clsx from "clsx";

import Button from "../../components/Button";
import useInView from "../../hook/useInView";
import useViewport from "../../hook/useViewPort";

export default function TopicContact() {
  const { ref, isInView } = useInView();
  const { width } = useViewport();

  return (
    <div
      className=""
      ref={ref}
      style={{
        background:
          "linear-gradient(174.86deg, #F4F9D3 -46.14%, #D6E0DA00 141.32%)",
      }}
    >
      <div className="mx-auto w-1920:w-[1500px] 2xl:w-[1300px] lg:w-[1000px] w-full lg:py-16 py-9 lg:px-5 px-0">
        <div className="grid lg:grid-cols-[2fr_3fr] grid-cols-1 gap-5">
          <div>
            <div className="px-5 lg:px-0">
              <img
                className="lg:w-20 w-10"
                src="/images/icon_contact.svg"
                alt=""
              />
            </div>
            <div>
              <h2
                className={clsx(
                  "lg:pt-5 pt-2 px-5 lg:px-0 lg:text-px32 text-px20 uppercase font-bold text-text-primary ",
                  {
                    "animate__animated animate__fadeInDown": isInView,
                  }
                )}
              >
                LIÊN HỆ VỚI CHÚNG TÔI ĐỂ ĐƯỢC HỖ TRỢ NHANH CHÓNG NHẤT
              </h2>
              <div className="pl-5 lg:pl-0 py-4 2xl:py-8">
                <div className="border-b-2 border-text-blue" />
              </div>
              <p
                className={clsx(
                  "text-px14 md:text-px18 text-justify text-[#3A5E81] lg:px-0 px-5 ",
                  { "animate__animated animate__fadeInLeftBig": isInView }
                )}
              >
                Mong muốn mang đến giá trị to lớn cho đối tác, SME có đội ngũ
                nhiệt tình, chu đáo luôn sẵn sàng giải đáp những thắc mắc, đưa
                ra những giải pháp, thiết kế có thể làm hài lòng những khách
                hàng khó tính nhất.
              </p>
            </div>
            <div
              className={`px-5 lg:px-0 lg:py-8 py-4 flex ${
                isInView ? "animate__animated animate__fadeInTopRight" : ""
              }`}
            >
              <button className="text-px14 lg:text-px18 px-3 lg:px-[34px] lg:py-[14px] py-2 w-fit bg-primary text-white">
                Liên hệ
              </button>
              <div className="bg-white flex justify-center items-center px-2">
                <img className="lg:w-6 w-3" src="/images/arrow.svg" />
              </div>
            </div>
          </div>
          <div className="px-5 lg:px-0">
            <div
              className="lg:rounded-bl-[40px] rounded-bl-[10px] lg:p-[20px_0_20px_20px] p-[10px_0_10px_12px] h-full"
              style={
                width >= 1024
                  ? {
                      backgroundImage:
                        "linear-gradient(90deg, #FFE81D 100%, #FFFFFF 0%), linear-gradient(90deg,#FFE81D 0px, #FFE81D 20px, transparent 0px, transparent 30px, #FFE81D 0px, #FFE81D 80px, transparent 0px, transparent 90px, #FFE81D 0px, #FFE81D 100%)",
                      backgroundRepeat: "no-repeat, no-repeat",
                      backgroundSize: "80% 40%, 40% 10px",
                      backgroundPosition: "0% 100%, 100% 0",
                    }
                  : {
                      backgroundImage:
                        "linear-gradient(90deg, #FFE81D 100%, #FFFFFF 0%), linear-gradient(90deg,#FFE81D 0px, #FFE81D 6px, transparent 0px, transparent 10px, #FFE81D 0px, #FFE81D 25px, transparent 0px, transparent 29px, #FFE81D 0px, #FFE81D 100%)",
                      backgroundRepeat: "no-repeat, no-repeat",
                      backgroundSize: "80% 40%, 40% 4px",
                      backgroundPosition: "0% 100%, 100% 0",
                    }
              }
            >
              <div className="h-full">
                <img
                  src={`images/images_contact.jpg`}
                  alt="contacer"
                  className="h-full w-full object-fill lg:rounded-[30px] rounded-[10px]"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
