import clsx from "clsx";
import { useEffect, useState } from "react";
import useInView from "../../hook/useInView";
import Button from "../Button";
import Title from "./Title";

export default function TopicAction() {
  const { ref, isInView } = useInView();
  const [categories, setCategories] = useState<any[]>([]);

  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    const res = await (await fetch("api/categories")).json();
    setCategories(res.list);
  };

  return (
    <div>
      <div className="h-full bg-[#34556C]" ref={ref}>
        <div className="mx-auto w-1920:w-[1500px] 2xl:w-[1300px] lg:w-[1000px] py-24 px-5">
          <div className="grid lg:grid-cols-[3fr_2fr] grid-cols-1 2xl:gap-x-12 gap-x-2 gap-y-5">
            <div className="flex items-center">
              <img
                src="/images/icon_title.svg"
                className="2xl:w-20 lg:w-14 w-8 2xl:mr-5 mr-2"
              />
              <div className="text-white font-bold uppercase text-px20 lg:text-px40 2xl:text-px48">
                LĨNH VỰC HOẠT ĐỘNG
              </div>
            </div>
            <div className="ml-auto w-2/5 lg:w-full">
              <div className="ml-auto w-2/5 bg-primary lg:h-3 h-1" />
              <div className="lg:mt-4 mt-1 bg-primary lg:h-3 h-1" />
            </div>
          </div>
          <div className="">
            <h4 className="font-semibold lg:text-px32 text-px16 lg:pt-16 py-10 text-white-color">
              Chế tạo & cung cấp thiết bị ngành
            </h4>
            <div className="grid lg:grid-cols-5 grid-cols-2 lg:gap-12 gap-4">
              {categories.map((item) => {
                return (
                  <div key={item.id} className="flex flex-col items-center">
                    <div
                      className={clsx(
                        "w-full aspect-square rounded-[10px] border-2 border-solid border-primary flex items-center justify-center cursor-pointer",
                        {
                          "animate__animated animate__fadeInDown": isInView,
                        }
                      )}
                    >
                      <img src={item.imageUrl} alt="" />
                    </div>
                    <p
                      className={clsx(
                        "lg:text-px20 text-px14 text-center line-clamp-2 text-white-color pt-4 cursor-pointer",
                        { "animate__animated animate__flash": isInView }
                      )}
                    >
                      {item.nameVi}
                    </p>
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
