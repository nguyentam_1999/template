import clsx from "clsx";
import useInView from "../../hook/useInView";
import Button from "../Button";
import { useEffect, useMemo, useRef } from "react";
import Title from "./Title";

export default function TopicSuccess() {
  const { ref, isInView } = useInView();
  const refFrame = useRef<HTMLIFrameElement>(null);

  useEffect(() => {
    if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
      create_iframe();
    }
  }, [refFrame.current]);

  function create_iframe() {
    if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
      refFrame.current?.classList.add("ios");
    }
  }

  return (
    <div className="" ref={ref}>
      <div className="mx-auto w-1920:w-[1500px] 2xl:w-[1300px] lg:w-[1000px] lg:py-24 py-8 px-5">
        <div className="grid lg:grid-cols-[3fr_2fr] grid-cols-1 2xl:gap-x-12 gap-x-4 gap-y-5">
          <div
            className={`flex flex-col ${
              isInView ? "animate__animated animate__fadeInLeft" : ""
            }`}
          >
            <div className="flex-1">
              <img src="/images/icon_title.svg" className="lg:w-20 w-8" />
              <div
                className={clsx(
                  "text-text-primary font-bold uppercase text-px20 lg:text-px48 lg:py-6 py-1"
                )}
              >
                THÀNH CÔNG CỦA BẠN LÀ ĐỊNH HƯỚNG CỦA CHÚNG TÔI
              </div>
            </div>
            <div className="pt-3">
              <iframe
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                scrolling="no"
                allowFullScreen={true}
                className="w-full aspect-video object-cover"
                width="100%"
                // height="315"
                src={`https://www.youtube.com/embed/${
                  true ? "0sIktMTze1Y" : "aUatyOjy3A8"
                }`}
                title="Anfico"
              ></iframe>
            </div>
          </div>

          <div
            className={`flex flex-col ${
              isInView ? "animate__animated animate__fadeInRight" : ""
            }`}
          >
            <div className="bg-primary 2xl:h-8 h-4"></div>
            <div className="flex-1 lg:py-5 py-1">
              <div>
                <img className="w-full" src="/images/pictures/picture01.png" />
              </div>
              <div
                className={clsx(
                  "text-px14 lg:text-px18 text-justify text-text-secondary py-2"
                )}
              >
                <p>
                  Với tâm huyết của ban lãnh đạo, kinh nghiệm và quyết tâm của
                  toàn thể đội ngũ kỹ thuật, công nhân viên, chúng tôi luôn tạo
                  ra những sản phẩm có chất lượng, thẩm mỹ cao. Bên cạnh đó
                  không ngừng cập nhật nâng cấp công nghệ, quy trình sản xuất để
                  đáp ứng được mọi yêu cầu của Quý khách hàng gần xa cả về chất
                  lượng và tiến độ.
                </p>
                <p>
                  Công ty chú trọng tuyệt đối đến chất lượng sản phẩm thông qua
                  việc lựa chọn nhà cung cấp nguyên liệu uy tín, đầu tư trang
                  thiết bị máy móc hiện đại cùng với những kỹ sư giàu kinh
                  nghiệm, đội ngũ công nhân lành nghề và hệ thống quản lý khoa
                  học. Chúng tôi tự tin khẳng định sản phẩm của mình có thể sánh
                  ngang với các sản phẩm thuộc phân khúc cao cấp đến từ châu Âu.
                </p>
              </div>
            </div>
            <div className="flex">
              <button className="text-px14 lg:text-px18 px-3 lg:px-[34px] lg:py-[14px] py-2 w-fit bg-primary text-white">
                Xem thêm
              </button>
              <div className="bg-white flex justify-center items-center px-2">
                <img className="lg:w-6 w-3" src="/images/arrow.svg" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
