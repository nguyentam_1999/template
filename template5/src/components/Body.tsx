import React from 'react'
import Banner from './Home/Banner'
import TopicAction from './Home/TopicAction'
import TopicContact from './Home/TopicContact'
import TopicCustomer from './Home/TopicCustomer'
import SliderProject from './Home/TopicProject'
import TopicSuccess from './Home/TopicSuccess'

const Body = () => {
  return (
    <div className='mt-[50px] xl:mt-[123px] bg-[#D6E0DA30]'>
        <Banner />
        <div>
          <TopicSuccess />
          <TopicAction />
          <SliderProject />
          <TopicCustomer />
          <TopicContact />
        </div>
        
    </div>
  )
}

export default Body