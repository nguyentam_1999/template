import React from "react";
import clsx from "clsx";

type Props = {
  title: string;
  isInView: boolean;
};

const Title = ({ title, isInView }: Props) => {
  return (
    <div className="">
      <div
        className={clsx(
          " text-text-primary font-bold uppercase 2xl:leading-[35px]  text-px20 md:text-[32px] flex items-center xl:text-[34px] 2xl:text-[48px]",
          { "animate__animated animate__fadeInRight": isInView }
        )}
      >
        {title}
      </div>
    </div>
  );
};

export default Title;
