import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
// import "swiper/modules/grid";
import "swiper/css/navigation";
import { Autoplay, Navigation, Pagination } from "swiper";
import React, { useMemo, useState, useEffect } from "react";
import Title from "./Title";
import useInView from "../../hook/useInView";
import useViewport from "../../hook/useViewPort";
const LIMIT = 6;

export default function SliderProject() {
  const [projectListActive, setProject] = useState<any[]>([]);
  const { isInView, ref } = useInView();
  const navigationPrevRef = React.useRef(null);
  const navigationNextRef = React.useRef(null);
  const paginationRef = React.useRef(null);
  const { width } = useViewport();

  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    const res = await (await fetch("api/project/priority")).json();

    setProject(res);
  };

  return (
    <div className="lg:pt-20 pt-8" ref={ref}>
      <div className="mx-auto w-1920:w-[1500px] 2xl:w-[1300px] lg:w-[1000px] lg:py-10 px-5">
        <div
          className={`flex items-center ${
            isInView ? "animate__animated animate__fadeInRight" : ""
          }`}
        >
          <img
            src="/images/icons/icon_title2.svg"
            className="lg:h-8 h-4 2xl:mr-5 mr-2"
          />
          <div className="text-text-primary font-bold uppercase text-px20 lg:text-px48">
            DỰ ÁN
          </div>
        </div>
      </div>
      <div className="grid w-1920:grid-cols-[1fr_1500px_1fr] 2xl:grid-cols-[1fr_1300px_1fr] lg:grid-cols-[1fr_1000px_1fr] grid-cols-1">
        <div className="lg:block hidden">
          <div className="w-full h-full flex items-center justify-center">
            <img
              ref={navigationPrevRef}
              className="w-[10px] lg:w-[23px] cursor-pointer"
              src="/images/arrowLeft_icon.svg"
              alt=""
            />
          </div>
        </div>
        <div className="px-5">
          <Swiper
            slidesPerView={width >= 1024 ? 3 : 1}
            spaceBetween={20}
            centeredSlides={true}
            loop={true}
            // loopFillGroupWithBlank={true}
            autoplay={{
              delay: 2500,
              disableOnInteraction: false,
            }}
            modules={[Navigation, Autoplay]}
            className="h-auto"
          >
            {projectListActive.map((item, index) => {
              return (
                <SwiperSlide className="" key={item.id}>
                  <div className="">
                    <div className="">
                      <img
                        src={`${item?.avatarUrl}`}
                        className="w-full aspect-video max-h-[500px] rounded-[10px]"
                        alt={item?.avatarPath ?? ""}
                      />
                    </div>
                    <div className="flex items-center justify-center py-5 px-5">
                      <p className="lg:text-px20 text-px16 font-bold text-center text-[#144E8C] line-clamp-2">
                        {item.titleVi}
                      </p>
                    </div>
                  </div>
                </SwiperSlide>
              );
            })}
          </Swiper>
        </div>
        <div className="lg:block hidden">
          <div className="w-full h-full flex items-center justify-center">
            <img
              ref={navigationNextRef}
              className="w-[10px] lg:w-[23px] cursor-pointer"
              src="/images/arrowRight_icon.svg"
              alt=""
            />
          </div>
        </div>
      </div>
    </div>
  );
}
