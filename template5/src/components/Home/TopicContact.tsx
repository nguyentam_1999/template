import clsx from "clsx";

import Button from "../../components/Button";
import useInView from "../../hook/useInView";
import useViewport from "../../hook/useViewPort";

export default function TopicContact() {
  const { ref, isInView } = useInView();
  const { width } = useViewport();
  return (
    <div
      className="2xl:pt-52 lg:pt-32 lg:pb-0 py-12"
      ref={ref}
      style={
        width >= 1024
          ? {
              backgroundImage:
                "linear-gradient(180deg, #6D959E 100%, #ffffff 0%), url(/images/pictures/bg-dotted2.svg)",
              backgroundSize: "70% 88%, auto 50px",
              backgroundPosition: "0 0, 34% 98%",
              backgroundRepeat: "no-repeat",
            }
          : {
              backgroundImage:
                "linear-gradient(180deg, #6D959E 100%, #ffffff 0%), url(/images/pictures/bg-dotted2.svg)",
              backgroundSize: "100% 70%, auto 50px",
              backgroundPosition: "0 0, calc(100% - 20px) 95%",
              backgroundRepeat: "no-repeat",
            }
      }
    >
      <div className="mx-auto w-1920:w-[1500px] 2xl:w-[1300px] lg:w-[1000px] w-full lg:px-5 px-0">
        <div className="grid lg:grid-cols-[1fr_2fr] grid-cols-1 gap-8">
          <div className="lg:-mt-4">
            <div>
              <h2
                className={clsx(
                  "px-5 lg:px-0 2xl:text-px32 lg:text-px24 text-px20 uppercase font-bold text-white",
                  {
                    "animate__animated animate__fadeInDown": isInView,
                  }
                )}
              >
                LIÊN HỆ VỚI CHÚNG TÔI ĐỂ ĐƯỢC HỖ TRỢ NHANH CHÓNG NHẤT
              </h2>
              <p
                className={clsx(
                  "text-px14 md:text-px18 text-justify text-white lg:px-0 py-4 px-5 ",
                  { "animate__animated animate__fadeInLeftBig": isInView }
                )}
              >
                Mong muốn mang đến giá trị to lớn cho đối tác, SME có đội ngũ
                nhiệt tình, chu đáo luôn sẵn sàng giải đáp những thắc mắc, đưa
                ra những giải pháp, thiết kế có thể làm hài lòng những khách
                hàng khó tính nhất.
              </p>
            </div>
            <div
              className={`px-5 lg:px-0 2xl:py-8 py-0 flex ${
                isInView ? "animate__animated animate__fadeInTopRight" : ""
              }`}
            >
              <button className="text-px14 lg:text-px18 px-3 lg:px-[34px] lg:py-[14px] py-2 w-fit bg-primary text-white">
                Liên hệ
              </button>
              <div className="bg-white flex justify-center items-center px-2">
                <img className="lg:w-6 w-3" src="/images/arrow.svg" />
              </div>
            </div>
          </div>
          <div className="px-5 lg:px-0">
            <div className="">
              <div className="w-3/4 lg:w-full">
                <img
                  src={`images/pictures/picture04.png`}
                  alt="contacer"
                  className="aspect-[4/3]"
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
