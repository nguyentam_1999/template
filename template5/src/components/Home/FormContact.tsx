import React, { useContext } from "react";

export const FormContact = () => {
  return (
    <form className="mt-[16px] text-inherit sc991:flex  sc991:flex-col text-normalgit max-w-sm">
      <div>
        <input
          type="text"
          name="customerName"
          placeholder="Họ và tên"
          className="w-full input outline-none border-b bg-transparent border-b-white focus:border-b-lightGreen duration-200"
        />
      </div>

      <div className="mt-[16px]">
        <input
          type="text"
          name="phone"
          placeholder="Số điện thoại"
          className="w-full input outline-none border-b bg-transparent border-b-white focus:border-b-lightGreen duration-200"
        />
      </div>

      <div className="mt-[16px]">
        <input
          type="email"
          name="email"
          placeholder="Email"
          className="w-full input outline-none border-b bg-transparent border-b-white focus:border-b-lightGreen duration-200"
        />
      </div>

      <div className="mt-[16px]">
        <input
          type="text"
          name="content"
          placeholder="Nội dung cần được tư vấn"
          className="w-full input outline-none border-b bg-transparent border-b-white focus:border-b-lightGreen duration-200"
        />
      </div>
      <div className="mt-[16px]">
        <div className="w-fit lg:rounded-[10px] rounded border border-[#0E2C99] translate-x-2 translate-y-2">
          <button
            className="-translate-x-2 -translate-y-2 w-[134px] h-[24px] rounded lg:w-[210px] lg:h-[40px] lg:rounded-[10px] text-[12px] lg:text-[18px] bg-[#0E2C99] after:top-1 after:left-1 lg:after:top-[7px] lg:after:left-[7px]"
            type="submit"
          >
            Tư vấn ngay
          </button>
        </div>
      </div>
    </form>
  );
};
