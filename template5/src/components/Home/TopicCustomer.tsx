import useInView from "../../hook/useInView";
import Title from "./Title";
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
// import "swiper/modules/grid";
import "swiper/css/navigation";
import { Autoplay, Navigation, Grid } from "swiper";
import React, { useMemo, useState, useEffect } from "react";
import useViewport from "../../hook/useViewPort";

const TopicCustomer = () => {
  const { isInView, ref } = useInView();
  const [partners, setPartners] = useState<any[]>([]);

  const { width } = useViewport();

  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    const res = await (await fetch("api/partner")).json();
    setPartners(res);
  };

  const navigationPrevRef = React.useRef(null);
  const navigationNextRef = React.useRef(null);

  return (
    <div className="lg:pt-16" ref={ref}>
      <div className="mx-auto w-1920:w-[1500px] 2xl:w-[1300px] lg:w-[1000px] lg:py-10 px-5">
        <div
          className={`flex items-center ${
            isInView ? "animate__animated animate__fadeInRight" : ""
          }`}
        >
          <img
            src="/images/icons/icon_title2.svg"
            className="lg:h-8 h-4 2xl:mr-5 mr-2"
          />
          <div className="text-text-primary font-bold uppercase text-px20 lg:text-px48">
            KHÁCH HÀNG
          </div>
        </div>
      </div>
      <div
        className="lg:py-20"
        style={{
          backgroundImage: "url(/images/pictures/picture03.png), linear-gradient(180deg, #04A004 100%, #ffffff 0%)",
          backgroundSize: "100% 100%",
          backgroundBlendMode: "multiply",
        }}
      >
        <div className="flex items-center lg:py-14 py-10 lg:px-5">
          <div className="lg:block hidden flex-1">
            <div className="w-full h-full flex items-center justify-center">
              <img
                ref={navigationPrevRef}
                className="w-[10px] lg:w-[23px] cursor-pointer"
                src="/images/arrowLeft_icon.svg"
                alt=""
              />
            </div>
          </div>
          <div className="w-1920:w-[1500px] 2xl:w-[1300px] lg:w-[1000px] w-full px-5">
            <Swiper
              slidesPerView={3}
              spaceBetween={width >= 1024 ? 42 : 10}
              loop={true}
              loopFillGroupWithBlank={true}
              autoplay={{
                delay: 2500,
                disableOnInteraction: false,
              }}
              navigation={{
                // Both prevEl & nextEl are null at render so this does not work
                prevEl: navigationPrevRef.current,
                nextEl: navigationNextRef.current,
              }}
              modules={[Navigation, Autoplay]}
              className="slider-company-home"
            >
              {partners.map((item, index) => {
                return (
                  <SwiperSlide className="py-1" key={item.id}>
                    <div className="w-full aspect-video shadow bg-white p-5 flex items-center justify-center">
                      <img
                        src={item?.imageUrl ?? ""}
                        className="max-h-[90%] object-contain w-[90%] h-full"
                        alt={item?.imageUrl ?? ""}
                      />
                    </div>
                  </SwiperSlide>
                );
              })}
            </Swiper>
          </div>
          <div className="lg:block hidden flex-1">
            <div className="w-full h-full flex items-center justify-center">
              <img
                ref={navigationNextRef}
                className="w-[10px] lg:w-[23px] cursor-pointer"
                src="/images/arrowRight_icon.svg"
                alt=""
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TopicCustomer;
