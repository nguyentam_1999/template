import clsx from "clsx";
import useInView from "../../hook/useInView";
import Button from "../Button";
import { useEffect, useMemo, useRef } from "react";
import Title from "./Title";
import useViewport from "../../hook/useViewPort";

export default function TopicSuccess() {
  const { ref, isInView } = useInView();
  const { width } = useViewport();
  const refFrame = useRef<HTMLIFrameElement>(null);

  useEffect(() => {
    if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
      create_iframe();
    }
  }, [refFrame.current]);

  function create_iframe() {
    if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
      refFrame.current?.classList.add("ios");
    }
  }

  return (
    <div
      className=""
      style={
        width >= 1920
          ? {
              backgroundImage:
                "url(/images/pictures/picture02.png), linear-gradient(173.77deg, #04A004 100%, #ffffff 0%), url(/images/pictures/bg-dotted1.svg)",
              backgroundSize: "100% 600px, calc(1500px / 10 * 6) 100%, auto auto",
              backgroundPosition:
                "0 0, calc((100vw - 1500px) / 2 + 1500px * 4 / 10) 400px, calc((100vw - 1500px) / 2 + 200px) 650px",
              backgroundRepeat: "no-repeat",
              backgroundBlendMode: "multiply",
            }
          : width >= 1536
          ? {
              backgroundImage:
                "url(/images/pictures/picture02.png), linear-gradient(173.77deg, #04A004 100%, #ffffff 0%), url(/images/pictures/bg-dotted1.svg)",
              backgroundSize: "100% 500px, calc(1300px / 10 * 6) 100%, auto auto",
              backgroundPosition:
                "0 0, calc((100vw - 1300px) / 2 + 1300px * 4 / 10) 300px, calc((100vw - 1300px) / 2 + 140px) 550px",
              backgroundRepeat: "no-repeat",
              backgroundBlendMode: "multiply",
            }
          : width >= 1024
          ? {
              backgroundImage:
                "url(/images/pictures/picture02.png), linear-gradient(173.77deg, #04A004 100%, #ffffff 0%), url(/images/pictures/bg-dotted1.svg)",
              backgroundSize: "100% 400px, calc(1000px / 10 * 6) 100%, auto auto",
              backgroundPosition:
                "0 0, calc((100vw - 1000px) / 2 + 1000px * 4 / 10) 200px, calc((100vw - 1000px) / 2 + 50px) 450px",
              backgroundRepeat: "no-repeat",
              backgroundBlendMode: "multiply",
            }
          : {
              backgroundImage: "url(/images/pictures/picture02.png), url(/images/pictures/bg-dotted3.svg), url(/images/pictures/bg-dotted3.svg)",
              backgroundSize: "100% 400px, auto auto, auto auto",
              backgroundPosition: "0 0, calc(100% - 20px) calc(400px + 80px), calc(100% - 20px) calc(400px + 160px)",
              backgroundRepeat: "no-repeat",
              backgroundBlendMode: "multiply",
            }
      }
      ref={ref}
    >
      <div className="">
        <div className="mx-auto w-1920:w-[1500px] 2xl:w-[1300px] lg:w-[1000px]">
          <div className="grid lg:grid-cols-[4fr_6fr] grid-cols-1">
            <div className="w-1920:h-[600px] 2xl:h-[500px] lg:h-[400px] flex items-end lg:px-0 px-16 justify-center lg:py-0 pt-14 pb-7">
              <iframe
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                scrolling="no"
                allowFullScreen={true}
                className="w-full max-w-[700px] aspect-video object-cover"
                width="100%"
                // height="315"
                src={`https://www.youtube.com/embed/${
                  true ? "0sIktMTze1Y" : "aUatyOjy3A8"
                }`}
                title="Anfico"
              ></iframe>
            </div>
            <div className="w-[calc(100%-64px)] lg:w-full 2xl:px-14 lg:px-8 px-16 py-4 lg:py-0 lg:pb-10 bg-[#036A02cc] lg:bg-transparent ">
              <div className="w-1920:mt-[400px] 2xl:mt-[300px] lg:mt-[200px]">
                <div className="lg:h-[200px] flex items-center">
                  <div
                    className={clsx(
                      "text-white font-bold uppercase text-px20 lg:text-px40 w-1920:text-px48 lg:py-6 py-1"
                    )}
                  >
                    THÀNH CÔNG CỦA BẠN LÀ ĐỊNH HƯỚNG CỦA CHÚNG TÔI
                  </div>
                </div>
              </div>
              <div
                className={clsx(
                  "text-px14 lg:text-px18 text-justify text-white py-4"
                )}
              >
                <p>
                  Với tâm huyết của ban lãnh đạo, kinh nghiệm và quyết tâm của
                  toàn thể đội ngũ kỹ thuật, công nhân viên, chúng tôi luôn tạo
                  ra những sản phẩm có chất lượng, thẩm mỹ cao. Bên cạnh đó
                  không ngừng cập nhật nâng cấp công nghệ, quy trình sản xuất để
                  đáp ứng được mọi yêu cầu của Quý khách hàng gần xa cả về chất
                  lượng và tiến độ.
                </p>
                <p>
                  Công ty chú trọng tuyệt đối đến chất lượng sản phẩm thông qua
                  việc lựa chọn nhà cung cấp nguyên liệu uy tín, đầu tư trang
                  thiết bị máy móc hiện đại cùng với những kỹ sư giàu kinh
                  nghiệm, đội ngũ công nhân lành nghề và hệ thống quản lý khoa
                  học. Chúng tôi tự tin khẳng định sản phẩm của mình có thể sánh
                  ngang với các sản phẩm thuộc phân khúc cao cấp đến từ châu Âu.
                </p>
              </div>
              <div className="flex">
                <button className="text-px14 lg:text-px18 px-3 lg:px-[34px] lg:py-[14px] py-2 w-fit bg-[#0E2C99] text-white">
                  Xem thêm
                </button>
                <div className="bg-white flex justify-center items-center px-2">
                  <img className="lg:w-6 w-3" src="/images/arrow.svg" />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
